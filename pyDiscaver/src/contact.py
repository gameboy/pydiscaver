# -*- coding: UTF-8 -*-

import json


class ContactList(list):
    def append(self, item):
        if not isinstance(item, Contact):
            raise TypeError('item is not of type {}'.format(Contact.__name__))
        super(ContactList, self).append(item)


class ContactResources(object):
    def __init__(self, name, resource):
        self.name = name
        self.resource = resource


class Contact(object):
    REQUIRED_CONTACT_PROPERTIES = ('alias', 'contact_name', 'file_id', 'host_notification_options',
                                   'host_notification_period', 'service_notification_options',
                                   'service_notification_period')
    ALL_CONTACT_PROPERTIES = REQUIRED_CONTACT_PROPERTIES + ('address1', 'address2', 'address3',
                                                            'address4', 'address5', 'address6',
                                                            'can_submit_commands', 'contactgroups',
                                                            'email', 'enable_access', 'groups',
                                                            'host_notification_cmds', 'host_notification_cmds_args',
                                                            'host_notifications_enabled', 'pager',
                                                            'password', 'realname', 'register',
                                                            'retain_nonstatus_information', 'retain_status_information',
                                                            'service_notification_cmds', 'service_notification_cmds_args',
                                                            'service_notifications_enabled', 'template')
    TO_SPLIT_PROPERTIES = ('host_notification_options', 'service_notification_options', 'contactgroups',
                           'groups')

    def __init__(self, **kwargs):
        self.custom_variables = {}
        for key, value in kwargs.iteritems():
            if key in Contact.ALL_CONTACT_PROPERTIES:
                if key in Contact.TO_SPLIT_PROPERTIES:
                    if not isinstance(value, list):
                        setattr(self, key, value)
                        continue
                setattr(self, key, value)
            if key.startswith('_'):
                self.custom_variables[key] = value

    def create_json(self):
        data = {}
        for key, value in self.__dict__.iteritems():
            if key == 'custom_variables':
                continue
            data[key] = value
        for key, value in self.custom_variables.iteritems():
            data[key] = value
        return json.dumps(data)

    def __str__(self):
        return "Contact: {0}".format(self.contact_name)
