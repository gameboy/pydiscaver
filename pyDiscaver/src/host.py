# -*- coding: UTF-8 -*-

import json


class HostList(list):
    host_list = []

    def __init__(self, **kwargs):
        super(HostList, self).__init__(**kwargs)

    def append(self, item):
        if not isinstance(item, Host):
            raise TypeError('item is not of type {}'.format(Host))
        super(HostList, self).append(item)

    def get_host_list(self):
        return [a.host_name for a in self]

class HostResources(object):
    def __init__(self, name, resource):
        self.name = name
        self.resource = resource


class Host(object):

    REQUIRED_HOST_PROPERTIES = ('address', 'alias', 'file_id', 'host_name', 'max_check_attempts', 'notification_interval', 'notification_options',
                                'notification_period', 'template')
    ALL_HOST_PROPERTIES = REQUIRED_HOST_PROPERTIES + ('2d_coords', 'action_url', 'active_checks_enabled', 'check_command', 'check_command_args',
                                                    'check_freshness', 'check_interval', 'check_period', 'contact_groups', 'contacts',
                                                    'display_name', 'event_handler', 'event_handler_args', 'event_handler_enabled',
                                                    'first_notification_delay', 'flap_detection_enabled', 'flap_detection_options', 'freshness_threshold',
                                                    'high_flap_threshold', 'hostgroups', 'icon_image', 'icon_image_alt', 'initial_state', 'low_flap_threshold',
                                                    'notes', 'notes_url', 'notifications_enabled', 'obsess_over_host', 'passive_checks_enabled',
                                                    'process_perf_data', 'register', 'retain_nonstatus_information', 'retain_status_information',
                                                    'retry_interval', 'stalking_options', 'statusmap_image')

    TO_SPLIT_PROPERTIES = ('notification_options', 'children', 'contact_groups', 'contacts', 'flap_detection_options', 'hostgroups', 'parents', 'stalking_options')
    DISABLED_PROPERTIES = ('children', 'parents')
    def __init__(self, **kwargs):
        self.custom_variables = {}
        for key in kwargs:
            if key in Host.ALL_HOST_PROPERTIES:
                if key in Host.TO_SPLIT_PROPERTIES:
                    if not isinstance(kwargs[key], list):
                        setattr(self, key, kwargs[key][0].split(','))
                        continue
                setattr(self, key, kwargs[key])
            if key.startswith('_'):
                self.custom_variables[key] = kwargs[key]

    def _shell_custom_variables(self, cv):
        custom = {}
        for c in cv:
            if c.startswith('_'):
                custom[c] = cv[c]
        return custom

    def create_json(self):
        data = {}
        for pro in self.__dict__:
            if pro == 'custom_variables':
                continue
            data[pro] = self.__dict__[pro]
        for cv in self.custom_variables:
            data[cv] = self.custom_variables[cv]
        return json.dumps(data)

    def print_custom_variables(self):
        for cv in self.custom_variables:
            print "{0} = {1}".format(cv, self.custom_variables[cv])

    def __str__(self):
        return "Host: {0}, address: {1}, alias: {2}".format(self.host_name, self.address, self.alias)