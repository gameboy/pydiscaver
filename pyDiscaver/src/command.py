# -*- coding: UTF-8 -*-

import json
import sys

class CommandList(list):
    command_list = []
    def __init__(self, **kwargs):
        super(CommandList, self).__init__(**kwargs)

    def append(self, item):
        if not isinstance(item, Command):
            raise TypeError, 'item is not of type {0}'.format(Command)
        super(CommandList, self).append(item)


class CommandResources(object):
    def __init__(self, name, resource):
        self.name = name
        self.resource = resource

    def __str__(self):
        return "Command name: {0}; Resource: {1}".format(self.name, self.resource)


class Command(object):
    
    ALL_COMMAND_PROPERTIES = ("command_line", "command_name", "file_id", "register")
    
    def __init__(self, **kwargs):
        if not "command_name" in kwargs:
            raise "Error: command_name field is required to update command"
            sys.exit(1)
            
        for key in kwargs:
            if key in Command.ALL_COMMAND_PROPERTIES:
                setattr(self, key, kwargs[key])

    def create_json(self):
        data = {}
        for pro in self.__dict__:
            data[pro] = self.__dict__[pro]            
        return json.dumps(data)
    
    def __str__(self):
        out = ''
        for key in self.__dict__:
            out += "{0}={1}\n".format(key.encode('ascii', 'ignore'), self.__dict__[key].encode('ascii', 'ignore'))
        return out