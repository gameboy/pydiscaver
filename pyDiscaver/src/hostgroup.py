import json


class HostGroupList(list):
    host_list = []

    def __init__(self, **kwargs):
        super(HostGroupList, self).__init__(**kwargs)

    def append(self, item):
        if not isinstance(item, HostGroup):
            raise TypeError('item is not of type {0}'.format(HostGroup.__name__))
        super(HostGroupList, self).append(item)


class HostGroupResources(object):
    def __init__(self, name, resource):
        self.name = name
        self.resource = resource


class HostGroup(object):
    REQUIRED_HOST_GROUP_PROPERTIES = ('file_id', 'hostgroup_name')
    ALL_HOST_GROUP_PROPERTIES = REQUIRED_HOST_GROUP_PROPERTIES + ('action_url', 'alias', 'notes', 'notes_url', 'register')
    CHECK_HOSTS_IN_GROUP = ('members',)

    def __init__(self, **kwargs):
        self.custom_variables = {}
        self.hostgroup_members = {}
        for key in kwargs:
            if key in HostGroup.ALL_HOST_GROUP_PROPERTIES:
                setattr(self, key, kwargs[key])
            if key.startswith('_'):
                self.custom_variables[key] = kwargs[key]
            if key in HostGroup.CHECK_HOSTS_IN_GROUP:
                self.hostgroup_members[key] = kwargs[key]

    def create_json(self):
        data = {}
        for pro in self.__dict__:
            if pro == 'custom_variables':
                continue
            data[pro] = self.__dict__[pro]
        for cv in self.custom_variables:
            data[cv] = self.custom_variables[cv]
        return json.dumps(data)

    def __str__(self):
        return "Host Group Name: {0} \
                Alias: {1} \
                File Id: {2} \
                Notes: {3} \
                Notes URL: {4} \
                Register: {5} \
                Host Group Members {6}\n".format(self.hostgroup_name,
                                                 self.alias,
                                                 self.file_id,
                                                 self.notes,
                                                 self.notes_url,
                                                 self.register,
                                                 self.hostgroup_members)