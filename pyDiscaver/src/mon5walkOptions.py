# -*- coding: UTF-8 -*-
import sys
#import configs

class RequiredOptionsError(Exception):
    pass


class Mon5WalkOptions(object):
    VERSION_NUMBER = '1.1.2'
    PROGRAM_NAME = "Mon5walk"
    DEVELOPER_NAME = "Maciej Gębarski"
    DEVELOPER_EMAIL = "maciej.gebarski@emca.pl"
    REQUIRED_SWITCHES = ('username', 'server', 'password', 'type', 'object')
    GENERAL_SWITCHES = REQUIRED_SWITCHES + ('save', 'no_log_screen', 'log_output', 'version')

    TYPE_SWITCHES_OPTIONS = ('config', 'status')
    OBJECT_SWITCHES_OPTIONS = ('command', 'host', 'service', 'contact', 'host-group', 'service-group', 'contact-group')
    OBJECT_CONFIG_SWITCHES_OPTIONS = ('list', 'update', 'get', 'add', 'import', 'delete', 'clone')


    ING_SWITCHES = ('add_host_ing', 'add_services_ing')

    def __init__(self, parser):
        self.parser = parser
        self.opts, self.args = self.__parse_options()
        self.password = self.opts.password
        self.username = self.opts.username
        self.server = self.opts.server
        self.object = self.opts.object
        self.type = self.opts.type
        #if self.opts.config:
        #    c = configs.load(self.opts.config)
        #    try:
        #        self.password = c['password']
        #    except:
        #        pass
        #    try:
        #        self.username = c['username']
        #    except:
        #        pass
        #    try:
        #        self.server = c['server']
        #    except:
        #        pass
        #    try:
        #        self.type = c['type']
        #    except:
        #        pass
        #    try:
        #        self.object = c['object']
        #    except:
        #        pass
        self.__set_switches()
        self.__set_extra_switch(self.opts)
        self.__check_required_switch()

    def __check_required_switch(self):
        if self.version:
            self.print_version()
            sys.exit(0)
        if not self.password or not self.server or not self.username or not self.object:
            print "Switch -s, -p, -u, -o is required. Use -h (--help) command for more detail"
            sys.exit(1)
        if self.object not in Mon5WalkOptions.OBJECT_SWITCHES_OPTIONS:
            print "Object must be: command, host, service, contact, host-group, service-group, contact-group"
            sys.exit(1)
        if self.type not in Mon5WalkOptions.TYPE_SWITCHES_OPTIONS:
            print "Type must be: config, status"
            sys.exit(1)
        if self.type == "config":
            if self.action not in Mon5WalkOptions.OBJECT_CONFIG_SWITCHES_OPTIONS:
                print "Action must by [list, update, get, add, import, delete, clone]"
                sys.exit(1)

    def __set_extra_switch(self, opts):
        self.save = opts.save
        self.no_log_screen = False if opts.no_log_screen else True
        self.log_output = opts.log_output
        self.version = opts.version

    def __parse_options(self):
        return self.parser.parse_args()

    def __set_switches(self):
        for key, item in self.opts.__dict__.items():
            if key not in Mon5WalkOptions.GENERAL_SWITCHES:
                #print "key: {0}, item: {1}".format(key, item)
                setattr(self, key, item)

    def print_version(self):
        print """
             ____ ____ ____ ____ ____ ____ ____ ____
            ||M |||o |||n |||5 |||W |||a |||l |||k ||
            ||__|||__|||__|||__|||__|||__|||__|||__||
            |/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|
             _______________________________________
            ||            VERSION  1.1.2           ||
            ||_____________________________________||
            |/_____________________________________\|
        """
        print "\t{0} is program which one help in management op5 monitor server.".format(Mon5WalkOptions.PROGRAM_NAME)
        print "\tTo view all it options pass -h (--help)."
        print "\tIt is developed by {0}".format(Mon5WalkOptions.DEVELOPER_NAME.encode('utf-8'))
        print "\tAll bugs or propositions please send to {0}".format(Mon5WalkOptions.DEVELOPER_EMAIL)
