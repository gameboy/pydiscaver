# -*- coding: UTF-8 -*-
import optparse


parser = optparse.OptionParser("usage: %prog -u [username] -p [password] -s [server] \
-o [object type] -a [action] ... [options]")

parser.add_option('-u', '--username',
                  help='[REQUIRED] op5 user name',
                  dest='username')

parser.add_option('-s', '--server',
                  help='[REQUIRED] op5 server address',
                  dest='server')

parser.add_option('-p', '--password',
                  help='[REQUIRED] op5 server password',
                  dest='password')

parser.add_option('-o', '--object',
                  help='[REQUIRED] op5 object [host, service, command, host-group, \
service-group, contact, contact-group]',
                  dest='object')

parser.add_option('-a', '--action',
                  help='[REQUIRED] action type [list, update, get, add, import, delete, clone]. \
[update, import, clone] require csv file',
                  dest='action')

parser.add_option('-t', '--type',
                  help='op5 api type [config, status], default [config]',
                  default='config',
                  dest='type')

parser.add_option('-c', '--config',
                  help='config file',
                  dest='config')

additional_group = optparse.OptionGroup(parser, 'Additional object options')

additional_group.add_option('--add-ping',
                            help='[Host: add, import] Add ping service to host',
                            dest='add_ping',
                            action='store_true',
                            default=False)

additional_group.add_option('--host',
                            help='[Service, Host: clone] Host name for witch clone services',
                            dest='host_to_clone_service',
                            metavar="<Host Name>")

additional_group.add_option('--delete-rdd-xml',
                            help='[Host, Service: delete] Delete RDD and XML file',
                            default=False,
                            dest='delete_rdd_xml',
                            action='store_true')

additional_group.add_option('--service-name',
                            help='[Service: clone] Clone services by specified name',
                            dest='service_name_to_clone',
                            metavar="<Service Name>")

additional_group.add_option('--hostgroup-name',
                            help='[Host: clone] Clone service by specified hostgroup name',
                            dest='hostgroup_name',
                            metavar="<HostGroup Name>")

additional_group.add_option('--with-services',
                            help="[Host: clone] Clone services with host",
                            default=False,
                            dest='with_service',
                            action='store_true')

additional_group.add_option('--unavailable-from',
                            help="[Host: clone] Clone hosts from a given downtime. d - days",
                            dest='unavailable_from',
                            metavar="<INT>+[d]"
                            )

additional_group.add_option('--servicegroup-name',
                            help="[Service: clone] Clone services from service group",
                            dest='service_group_name',
                            metavar="<ServiceGroupName>"
                            )

parser.add_option('--save',
                  help='Save changes on server automatically after complete',
                  dest='save',
                  default=False,
                  action='store_true')

parser.add_option('--log-output',
                  help='log output to file',
                  default=False,
                  dest='log_output',
                  metavar="<LOG FILE>")

parser.add_option('--no-log-screen',
                  help='Do not log to screen',
                  default=False,
                  dest='no_log_screen',
                  action='store_true')

parser.add_option('-v', '--version',
                  help='inforamtion about version',
                  dest='version',
                  default=False,
                  action='store_true')

parser.add_option_group(additional_group)

(opts, args) = parser.parse_args()