# -*- coding: UTF-8 -*-
import json


class ServiceGroupList(list):
    servicegroup_list = []

    def __init__(self, **kwargs):
        super(ServiceGroupList, self).__init__(**kwargs)

    def append(self, item):
        if not isinstance(item, ServiceGroup):
            raise TypeError('item is not of type {0}'.format(ServiceGroup.__name__))
        super(ServiceGroupList, self).append(item)


class ServiceGroupResources(object):
    def __init__(self, name, resource):
        self.name = name
        self.resource = resource


class ServiceGroup(object):
    REQUIRED_SERVICEGROUP_PROPERTIES = ('alias', 'file_id', 'servicegroup_name')
    ALL_SERVICEGROUP_PROPERTIES = REQUIRED_SERVICEGROUP_PROPERTIES + ('action_url',
                                                                      'notes',
                                                                      'notes_url',
                                                                      'register')
    CHECK_SERVICE_IN_GROUP = ('members',)

    def __init__(self, **kwargs):
        self.custom_variables = {}
        self.members = {}
        for key in kwargs:
            if key in ServiceGroup.ALL_SERVICEGROUP_PROPERTIES:
                setattr(self, key, kwargs[key])
            if key.startswith('_'):
                self.custom_variables[key] = kwargs[key]
            if key in ServiceGroup.CHECK_SERVICE_IN_GROUP:
                self.members[key] = kwargs[key]

    def create_json(self):
        data = {}
        for pro in self.__dict__:
            if pro == 'custom_variables':
                continue
            data[pro] = self.__dict__[pro]
        for cv in self.custom_variables:
            data[cv] = self.custom_variables[cv]
        return json.dumps(data)

    def __str__(self):
        return "Service Group Name: {0}".format(self.servicegroup_name)
