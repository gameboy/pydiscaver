# -*- coding: UTF-8 -*-

import json


class ContactGroupList(list):

    def append(self, item):
        if not isinstance(item, ContactGroup):
            raise TypeError('item is not of type {0}'.format(ContactGroup.__name__))
        super(ContactGroupList, self).append(item)


class ContactGroupResources(object):
    def __init__(self, name, resource):
        self.name = name
        self.resource = resource


class ContactGroup(object):
    REQUIRED_CONTACT_GROUP_PROPERTIES = ('alias', 'contactgroup_name', 'file_id')
    ALL_CONTACT_GROUP_PROPERTIES = REQUIRED_CONTACT_GROUP_PROPERTIES + ('register', )
    CHECK_CONTACTS_IN_GROUP = ('members', )

    def __init__(self, **kwargs):
        self.custom_variables = {}
        self.contactgroup_members = {}
        for key in kwargs:
            if key in ContactGroup.ALL_CONTACT_GROUP_PROPERTIES:
                setattr(self, key, kwargs[key])
            if key.startswith('_'):
                self.custom_variables[key] = kwargs[key]
            if key in ContactGroup.CHECK_CONTACTS_IN_GROUP:
                self.contactgroup_members[key] = kwargs[key]

    def create_json(self):
        data = {}
        for pro in self.__dict__:
            if pro == "custom_variables":
                continue
            data[pro] = self.__dict__[pro]
        for cv in self.custom_variables:
            data[cv] = self.custom_variables[cv]
        return  json.dumps(data)

    def __str__(self):
        return "Contact Group Name: {0}".format(self.contactgroup_name)

