# -*- coding: UTF-8 -*-
from parser import ParserCSV
from ..command import Command, CommandList
from types import NoneType
import sys


class ParserCommandCSV(ParserCSV):
    def __init__(self, **kwargs):
        super(ParserCommandCSV, self).__init__(**kwargs)

    def _create_command(self, command_data):
        return Command(**command_data)

    def parse(self):
        file = self._open_file()
        try:
            command_list = CommandList()
            counter = 0
            paterns = {}
            for line in file:
                if line == "\n":
                    continue
                line = line.rstrip()
                data_command = {}
                if not counter:
                    split = line.split(self._split_char)
                    for index, pro in enumerate(split):
                        if (pro in Command.ALL_COMMAND_PROPERTIES):
                            paterns[pro] = index
                        else:
                            print "WARNING: column {0} in file {1} is not valid host op5 api properties (skip it)".format(pro, self.file_path)
                    counter += 1
                    continue
                split = line.split(self._split_char)
                for key in paterns:
                    val = paterns[key]
                    if split[val]:
                        data_command[key] = split[val]
                command = self._create_command(data_command)
                command_list.append(command)
            return command_list

        except NoneType:
            print("File {0} probably is blank".format(self._file_path))
            sys.exit(1)

    def parse_to_csv(self, command_list):
        file = self._open_file_to_write()
        try:
            file.write(str("command_name;command_line;file_id;register\n"))
            for command in command_list:
                file.write(str("{0};{1};{2};{3}\n".format(command.command_name.encode('utf-8'),
                                                          command.command_line.encode('utf-8'),
                                                          command.file_id.encode('utf-8'),
                                                          command.register)))

            print "Write to file OK."
        except Exception, e:
            print "Some error in parse_to_scv: {0}".format(e)
            sys.exit(1)

    def parse_cli_command_data(self, data):
        try:
            v = {}
            tab = data.split(';')
            for a in tab:
                w = a.split(':')
                if not w[0] in Command.ALL_COMMAND_PROPERTIES:
                    print "Error, field {0} is not correct Command op5 properties".format(w[0])
                    sys.exit(1)
                v[w[0]] = w[1]
            return Command(**v)
        except:
            print "Bad command format. See manual !!"
            sys.exit(1)

