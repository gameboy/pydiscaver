# -*- coding: UTF-8 -*-
from ..host import Host, HostList
from parser import ParserCSV
from types import NoneType
import sys


class ParserHostCSV(ParserCSV):
    def __init__(self, **kwargs):
        super(ParserHostCSV, self).__init__(**kwargs)

    def _parse_host(self, split):
        host = Host()
        return host

    def _parse_to_tab(self, field):
        tab = field.split(',')
        return tab

    def _create_host(self, host_data):
        return Host(**host_data)

    def parse(self):
        file = self._open_file()
        try:
            host_list = HostList()
            counter = 0
            paterns = {}
            for line in file:
                line = line.rstrip()
                data_host = {}
                if not counter:
                    split = line.split(self._split_char)
                    for index, pro in enumerate(split):
                        if ((pro in Host.ALL_HOST_PROPERTIES) or pro.startswith('_') or pro == 'custom_variables'):
                            paterns[pro] = index
                        else:
                            print "WARNING: column {0} in file {1} is not valid host op5 api properties (skip it)".format(pro, self.file_path)
                    counter += 1
                    continue

                split = line.split(self._split_char)
                for key in paterns:
                    val = paterns[key]
                    try:
                        if key == "custom_variables":
                            ll = split[val]
                            if not ll:
                                continue
                            spll = ll.split('||')
                            for s in spll:
                                d = s.split(':')
                                data_host[d[0]] = d[1]
                            continue
                        if split[val] or key.startswith('_'):
                            if key in Host.TO_SPLIT_PROPERTIES:
                                split[val] = split[val].split(',')
                            data_host[key] = split[val]
                    except IndexError, e:
                        print "Something bad with your csv file. Check it!! {0}".format(e)
                        sys.exit(1)
                host = self._create_host(data_host)
                #print host.custom_variables
                host_list.append(host)
            return host_list
        except NoneType:
            print("File {0} prodably is blank".format(self._file_path))
            sys.exit(1)

    def parse_to_csv(self, host_list):
        file = self._open_file_to_write()
        try:
            strr = ";".join(Host.ALL_HOST_PROPERTIES)
            strr = strr + ";custom_variables"
            file.write(str(strr + "\n"))
            for host in host_list:
                host_parsed_line = ''
                for prop in Host.ALL_HOST_PROPERTIES:
                    try:
                        val = host.__dict__[prop]
                    except:
                        val = ''
                    if not host_parsed_line:
                        if not val:
                            val = ' '
                        host_parsed_line = val
                        continue
                    if isinstance(val, int):
                        val = str(val)
                    if isinstance(val, list):
                        val = ",".join(val)
                    host_parsed_line = ';'.join([host_parsed_line, val])
                host_parsed_line += ';'
                if host.custom_variables:
                    val = "||".join(['{0}:{1}'.format(key, value) for key, value in host.custom_variables.items()])
                    host_parsed_line = host_parsed_line + '{0}'.format(val)
                file.write(str("{0}\n".format(host_parsed_line.encode('utf-8'))))
            print "Write to file OK."
        except Exception, e:
            print "Some error in parse_to_scv: {0}".format(e)
            sys.exit(1)

    def parse_cli_host_data(self, data):
        try:
            v = {}
            tab = data.split(';')
            for a in tab:
                w = a.split(':')
                if not w[0] in Host.ALL_HOST_PROPERTIES:
                    print "Error, field {0} is not correct Host op5 properties".format(w[0])
                    sys.exit(1)
                v[w[0]] = w[1]
            return Host(**v)
        except Exception:
            print "Bad host format. See manual !!"
            sys.exit(1)