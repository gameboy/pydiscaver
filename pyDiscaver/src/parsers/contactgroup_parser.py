# -*- coding: UTF-8 -*-
from types import NoneType
from parser import ParserCSV
from ..contactgroup import ContactGroup, ContactGroupList
import sys


class ParserContactGroupCSV(ParserCSV):
    def _create_contactgroup(self, contactgroup_data):
        return ContactGroup(**contactgroup_data)

    def parse(self):
        file = self._open_file()
        try:
            contactgroup_list = ContactGroupList()
            counter = 0
            paterns = {}
            for line in file:
                line = line.rstrip()
                if not line:
                    continue
                data_contacgroup = {}
                if not counter:
                    split = line.split(self._split_char)
                    for index, pro in enumerate(split):
                        if ((pro in ContactGroup.ALL_CONTACT_GROUP_PROPERTIES) or pro.startswith('_')):
                            paterns[pro] = index
                        else:
                            print "WARNING: column {0} in file {1} is not valid contact group op5 api properties (skip it)".format(pro, self.file_path)
                    counter += 1
                    continue

                split = line.split(self._split_char)
                for key in paterns:
                    val = paterns[key]
                    try:
                        if split[val] or key.startswith('_'):
                            # if key in HostGroup.TO_SPLIT_PROPERTIES:
                            #     split[val] = split[val].split(',')
                            data_contacgroup[key] = split[val]
                    except IndexError, e:
                        print "Something bad with your csv file. Check it!!{0}".format(e)
                        sys.exit(1)
                contactgroup = self._create_contactgroup(data_contacgroup)
                contactgroup_list.append(contactgroup)
            return contactgroup_list
        except NoneType:
            print("File {0} prodably is blank".format(self._file_path))
            sys.exit(1)

    def parse_to_csv(self, contactgroup_list):
        file = self._open_file_to_write()
        try:
            strr = ";".join(ContactGroup.ALL_CONTACT_GROUP_PROPERTIES)
            file.write(str(strr + "\n"))
            for host in contactgroup_list:
                contactgroup_parsed_line = ''
                for prop in ContactGroup.ALL_CONTACT_GROUP_PROPERTIES:
                    try:
                        val = host.__dict__[prop]
                        #print "{0}: {1}".format(val, type(val))
                    except:
                        val = ''

                    if not contactgroup_parsed_line:
                        if not val:
                            val = ' '
                        contactgroup_parsed_line = val
                        continue
                    if isinstance(val, int):
                        val = str(val)
                    if isinstance(val, list):
                        val = ",".join(val)

                    # if not val or val == '\n' or val== u' ' or val is None or val is unicode(''):
                    #     print "val null"
                    contactgroup_parsed_line = ';'.join([contactgroup_parsed_line, val])
                file.write(str("{0}\n".format(contactgroup_parsed_line.encode('utf-8'))))
            print "Write to file OK."
        except Exception, e:
            print "Some error in parse_to_scv: {0}".format(e)
            sys.exit(1)

    def parse_cli_contacgroup_data(self, data):
        try:
            v = {}
            tab = data.split(';')
            for a in tab:
                w = a.split(':')
                if not w[0] in ContactGroup.ALL_CONTACT_GROUP_PROPERTIES:
                    print "Error, field {0} is not correct Contact Group op5 properties".format(w[0])
                    sys.exit(1)
                v[w[0]] = w[1]
            return ContactGroup(**v)
        except Exception, e:
            print "Bad host format. See manual {0}!!".format(e)
            sys.exit(1)