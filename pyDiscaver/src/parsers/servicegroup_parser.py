# -*- coding: UTF-8 -*-
from types import NoneType
from parser import ParserCSV
from ..servicegroup import ServiceGroup, ServiceGroupList
import sys


class ParserServiceGroupCSV(ParserCSV):
    def __init__(self, **kwargs):
        super(ParserServiceGroupCSV, self).__init__(**kwargs)

    def _create_servicegroup(self, servicegroup_data):
        return ServiceGroup(**servicegroup_data)

    def parse(self):
        file = self._open_file()
        try:
            servicegroup_list = ServiceGroupList()
            counter = 0
            paterns = {}
            for line in file:
                line = line.rstrip()
                if not line:
                    continue
                data_servicegroup = {}
                if not counter:
                    split = line.split(self._split_char)
                    for index, pro in enumerate(split):
                        if ((pro in ServiceGroup.ALL_SERVICEGROUP_PROPERTIES) or pro.startswith('_')):
                            paterns[pro] = index
                        else:
                            print "WARNING: column {0} in file {1} is not valid \
                                   service group op5 api properties (skip it)".format(pro, self.file_path)
                    counter += 1
                    continue
                split = line.split(self._split_char)
                for key, val in paterns.iteritems():
                    try:
                        if split[val] or key.startswith('_'):
                            data_servicegroup[key] = split[val]
                    except IndexError, e:
                        print "Something bad with your csv file. Check it!!{0}".format(e)
                        sys.exit(1)
                servicegroup = self._create_servicegroup(data_servicegroup)
                servicegroup_list.append(servicegroup)
            return servicegroup_list
        except NoneType:
            print("File {0} prodably is blank".format(self._file_path))
            sys.exit(1)

    def parse_cli_servicegroup_data(self, data):
        try:
            v = {}
            tab = data.split(';')
            for a in tab:
                w = a.split(':')
                if not w[0] in ServiceGroup.ALL_SERVICEGROUP_PROPERTIES:
                    print "Error, field {0} is not correct Service Group op5 properties".format(w[0])
                    sys.exit(1)
                v[w[0]] = w[1]
            return ServiceGroup(**v)
        except Exception, e:
            print "Bad service group format. See manual {0}!!".format(e)
            sys.exit(1)

    def parse_to_csv(self, servicegroups_list):
        file = self._open_file_to_write()
        try:
            strr = ";".join(ServiceGroup.ALL_SERVICEGROUP_PROPERTIES)
            file.write(str(strr + "\n"))
            for servicegroup in servicegroups_list:
                servicegroup_parsed_line = ''
                for prop in ServiceGroup.ALL_SERVICEGROUP_PROPERTIES:
                    try:
                        val = servicegroup.__dict__[prop]
                    except:
                        val = ''
                    if not servicegroup_parsed_line:
                        if not val:
                            val = ' '
                        servicegroup_parsed_line = val
                        continue
                    if isinstance(val, int):
                        val = str(val)
                    if isinstance(val, list):
                        val = ",".join(val)
                    servicegroup_parsed_line = ';'.join([servicegroup_parsed_line, val])
                file.write(str("{0}\n".format(servicegroup_parsed_line.encode('utf-8'))))
            print "Write to file OK."
        except Exception, e:
            print "Some error in parse_to_scv: {0}".format(e)
            sys.exit(1)