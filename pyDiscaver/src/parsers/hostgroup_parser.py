# -*- coding: UTF-8 -*-
from types import NoneType
from parser import ParserCSV
from ..hostgroup import HostGroup, HostGroupList
import sys


class ParserHostGroupCSV(ParserCSV):
    def __init__(self, **kwargs):
        super(ParserHostGroupCSV, self).__init__(**kwargs)

    def _create_hostgroup(self, hostgroup_data):
        return HostGroup(**hostgroup_data)

    def parse(self):
        file = self._open_file()
        try:
            host_list = HostGroupList()
            counter = 0
            paterns = {}
            for line in file:
                line = line.rstrip()
                if not line:
                    continue
                data_host = {}
                if not counter:
                    split = line.split(self._split_char)
                    for index, pro in enumerate(split):
                        if ((pro in HostGroup.ALL_HOST_GROUP_PROPERTIES) or pro.startswith('_')):
                            paterns[pro] = index
                        else:
                            print "WARNING: column {0} in file {1} is not valid host group op5 api properties (skip it)".format(pro, self.file_path)
                    counter += 1
                    continue

                split = line.split(self._split_char)
                for key in paterns:
                    val = paterns[key]
                    try:
                        if split[val] or key.startswith('_'):
                            # if key in HostGroup.TO_SPLIT_PROPERTIES:
                            #     split[val] = split[val].split(',')
                            data_host[key] = split[val]
                    except IndexError,e:
                        print "Something bad with your csv file. Check it!!{0}".format(e)
                        sys.exit(1)
                host = self._create_hostgroup(data_host)
                host_list.append(host)
            return host_list
        except NoneType:
            print("File {0} prodably is blank".format(self._file_path))
            sys.exit(1)

    def parse_to_csv(self, hostgroup_list):
        file = self._open_file_to_write()
        try:
            strr = ";".join(HostGroup.ALL_HOST_GROUP_PROPERTIES)
            file.write(str(strr + "\n"))
            for host in hostgroup_list:
                host_parsed_line = ''
                for prop in HostGroup.ALL_HOST_GROUP_PROPERTIES:
                    try:
                        val = host.__dict__[prop]
                    except:
                        val = ''
                    if not host_parsed_line:
                        if not val:
                            val = ' '
                        host_parsed_line = val
                        continue
                    if isinstance(val, int):
                        val = str(val)
                    if isinstance(val, list):
                        val = ",".join(val)
                    host_parsed_line = ';'.join([host_parsed_line, val])
                file.write(str("{0}\n".format(host_parsed_line.encode('utf-8'))))
            print "Write to file OK."
        except Exception, e:
            print "Some error in parse_to_scv: {0}".format(e)
            sys.exit(1)

    def parse_cli_hostgroup_data(self, data):
        try:
            v = {}
            tab = data.split(',')
            for a in tab:
                w = a.split(':')
                if not w[0] in HostGroup.ALL_HOST_GROUP_PROPERTIES:
                    print "Error, field {0} is not correct Service op5 properties".format(w[0])
                    sys.exit(1)
                v[w[0]] = w[1]
            return HostGroup(**v)
        except Exception, e:
            print "Bad host format. See manual {0}!!".format(e)
            sys.exit(1)