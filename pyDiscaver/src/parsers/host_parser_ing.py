# -*- coding: UTF-8 -*-
from ..host import Host, HostList
from parser import ParserCSV
from types import NoneType
import sys


class ParserHostINGCSV(ParserCSV):
    def __init__(self, **kwargs):
        super(ParserHostINGCSV, self).__init__(**kwargs)

    def _parse_host(self, split):
        host = Host()
        return host

    def _parse_to_tab(self, field):
        tab = field.split(',')
        return tab

    def _create_host(self, host_data):
        return Host(**host_data)

    def parse(self):
        file = self._open_file()
        try:
            host_list = HostList()
            for line in file:
                line = line.rstrip()
                if line.startswith("#"):
                    continue
                split = line.split(self._split_char)
                paterns = {
                    'address':  split[0] if len(split) > 0 else '',
                    'alias': split[1] if len(split) > 1 else '',
                    'file_id': split[2] if len(split) > 2 else '',
                    'host_name': split[3] if len(split) > 3 else '',
                    'max_check_attempts': split[4] if len(split) > 4 else '',
                    'notification_interval': split[5] if len(split) > 5 else '',
                    'notification_options': split[6] if len(split) > 6 else '',
                    'template': split[7] if len(split) > 7 else '',
                    'parents': split[8].split(',') if len(split) > 8 else [],
                    'hostgroups': split[9].split(',') if len(split) > 9 else [],
                    'children': split[10].split(',') if len(split) > 10 else [],
                    'contacts': split[11].split(',') if len(split) > 11 else [],
                    'contact_groups': split[12].split(',') if len(split) > 12 else [],
                }
                host = self._create_host(paterns)
                host_list.append(host)
            return host_list
        except NoneType:
            print("File {0} prodably is blank".format(self._file_path))
            sys.exit(1)
