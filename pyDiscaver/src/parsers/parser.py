# -*- coding: UTF-8 -*-
import sys

class ParserCSVError(Exception):

    def __init__(self, file):
        self.file = file

    def messages(self):
        print("Could not open {} file".format(self.file))


class Parser(object):
    def __init__(self, **kwargs):
        pass


class ParserCSV(Parser):
    sign = ".csv"

    def __init__(self, file_path, split_char,  **kwargs):
        self._file_path = file_path
        self._split_char = split_char
        super(ParserCSV, self).__init__(**kwargs)

    @property
    def file_path(self):
        return self._file_path

    def _open_file(self):
        try:
            descriptor = open(self.file_path, "r")
            return descriptor

        except IOError, e:
            print "Could not open {0} file".format(self.file_path)
            sys.exit(1)

    def _open_file_to_write(self):
        try:
            descriptor = open(self.file_path, "w")
            return descriptor

        except IOError, e:
            print "Could not open {0} file".format(self.file_path)
            sys.exit(1)