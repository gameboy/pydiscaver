# -*- coding: UTF-8 -*-

from types import NoneType
from ..service import Service, ServiceList
from parser import ParserCSV
import sys


class ParserServiceCSV(ParserCSV):
    def __init__(self, **kwargs):
        super(ParserServiceCSV, self).__init__(**kwargs)

    def _create_service(self, service_data):
        return Service(**service_data)

    def parse_cli_service_data(self, data):
        try:
            v = {}
            tab = data.split(';')
            for a in tab:
                w = a.split(':')
                if not w[0] in Service.ALL_SERVICE_PROPERTIES:
                    print "Error, field {0} is not correct Service op5 properties".format(w[0])
                    sys.exit(1)
                if w[0] in Service.TO_SPLIT_PROPERTIES:
                    w[1] = w[1].split(',')
                v[w[0]] = w[1]
            return Service(**v)
        except Exception:
            print "Bad host format. See manual !!"
            sys.exit(1)

    def parse_to_csv(self, service_list):
        file = self._open_file_to_write()
        try:
            strr = ";".join(Service.ALL_SERVICE_PROPERTIES)
            strr = strr + ";custom_variables"
            file.write(str(strr + "\n"))
            for service in service_list:
                service_parsed_line = ''
                for prop in Service.ALL_SERVICE_PROPERTIES:
                    try:
                        val = service.__dict__[prop]
                    except:
                        val = ''
                    if not service_parsed_line:
                        if not val:
                            val = ' '
                        service_parsed_line = val
                        continue
                    if isinstance(val, int):
                        val = str(val)
                    if isinstance(val, list):
                        val = ",".join(val)
                    service_parsed_line = ';'.join([service_parsed_line, val])
                service_parsed_line += ';'
                if service.custom_variables:
                    val = "|".join(['{0}:{1}'.format(key, value) for key, value in service.custom_variables.items()])
                    service_parsed_line = service_parsed_line + '{0}'.format(val)
                file.write(str("{0}\n".format(service_parsed_line.encode('utf-8'))))
            print "Write to file OK."
        except Exception, e:
            print "Some error in parse_to_scv: {0}".format(e)
            sys.exit(1)

    def parse(self):
        file = self._open_file()
        try:
            service_list = ServiceList()
            counter = 0
            paterns = {}
            for line in file:
                line = line.rstrip()
                data_service = {}
                if not counter:
                    split = line.split(self._split_char)
                    for index, pro in enumerate(split):
                        if ((pro in Service.ALL_SERVICE_PROPERTIES) or pro.startswith('_') or pro == 'custom_variables'):
                            paterns[pro] = index
                        else:
                            print "WARNING: column {0} in file {1} is not valid host op5 api properties (skip it)".format(pro, self.file_path)
                    counter += 1
                    continue

                split = line.split(self._split_char)
                for key in paterns:
                    val = paterns[key]
                    try:
                        if key == "custom_variables":
                            #print "a"
                            ll = split[val]
                            #print "b"
                            #print ll
                            if not ll:
                                continue
                            spll = ll.split('|')
                            for s in spll:
                                d = s.split(':')
                                data_service[d[0]] = d[1]
                            continue
                        if split[val] or key.startswith('_'):
                            if key in Service.TO_SPLIT_PROPERTIES:
                                split[val] = split[val].split(',')
                            data_service[key] = split[val]
                    except IndexError, e:
                        print "Something bad with your csv file. Check it!! {0}".format(e)
                        sys.exit(1)
                service = self._create_service(data_service)
                service_list.append(service)
            return service_list
        except NoneType:
            print("File {0} prodably is blank".format(self._file_path))
            sys.exit(1)