# -*- coding: UTF-8 -*-
from ..service import Service, ServiceList
from parser import ParserCSV
from types import NoneType
import sys


class ParserServiceINGCSV(ParserCSV):
    def __init__(self, **kwargs):
        super(ParserServiceINGCSV, self).__init__(**kwargs)

    def _create_service(self, service_data):
        return Service(**service_data)

    def parse(self):
        file = self._open_file()
        try:
            service_list = ServiceList()
            for line in file:
                line = line.rstrip()
                if line.startswith("#"):
                    continue
                split = line.split(self._split_char)
                paterns = {
                    'check_command': split[0],
                    'check_interval': split[1],
                    'check_period': split[2],
                    'file_id': split[3],
                    'host_name': split[4],
                    'max_check_attempts': split[5],
                    'notification_interval': split[6],
                    'notification_options': split[7],
                    'notification_period': split[8],
                    'retry_interval': split[9],
                    'service_description': split[10],
                    'template': split[11],
                    'check_command_args': split[12],
                    'contacts': split[13].split(',') if len(split) > 13 else [],
                    'contact_groups': split[14].split(',') if len(split) > 14 else [],
                    'servicegroups': split[15].split(',') if len(split) > 15 else [],
                }
                key = 16
                while 1:
                    try:
                        vc = split[key].split(':')
                        paterns[vc[0]] = vc[1]
                        key += 1
                    except IndexError:
                        break
                service = self._create_service(paterns)
                service_list.append(service)
            return service_list
        except NoneType:
            print("File {0} prodably is blank".format(self._file_path))
            sys.exit(1)
