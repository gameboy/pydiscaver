# -*- coding: UTF-8 -*-

from ..contact import Contact, ContactList
from parser import ParserCSV
from types import NoneType
import sys


class ParserContactCSV(ParserCSV):
    def _create_contact(self, contact_data):
        return Contact(**contact_data)

    def parse_cli_contact_data(self, data):
        try:
            v = {}
            tab = data.split(';')
            for a in tab:
                w = a.split(':')
                if not w[0] in Contact.ALL_CONTACT_PROPERTIES:
                    print "Error, field {0} is not correct Contact op5 properties".format(w[0])
                    sys.exit(1)
                if w[0] in Contact.TO_SPLIT_PROPERTIES:
                    w[1] = w[1].split(',')
                v[w[0]] = w[1]
            return Contact(**v)
        except Exception:
            print "Bad contact format. See manual !!"
            sys.exit(1)

    def parse(self):
        file = self._open_file()
        try:
            contact_list = ContactList()
            counter = 0
            paterns = {}
            for line in file:
                line = line.rstrip()
                data_contact = {}
                if not counter:
                    split = line.split(self._split_char)
                    for index, pro in enumerate(split):
                        if ((pro in Contact.ALL_CONTACT_PROPERTIES) or pro.startswith('_')):
                            paterns[pro] = index
                        else:
                            print "WARNING: column {0} in file {1} is not valid contact op5 api properties (skip it)".format(pro, self.file_path)
                    counter += 1
                    continue

                split = line.split(self._split_char)
                for key in paterns:
                    val = paterns[key]
                    try:
                        if split[val] or key.startswith('_'):
                            if key in Contact.TO_SPLIT_PROPERTIES:
                                split[val] = split[val].split(',')
                            data_contact[key] = split[val]
                    except IndexError:
                        print "Something bad with your csv file. Check it!!"
                        sys.exit(1)
                contact = self._create_contact(data_contact)
                contact_list.append(contact)
            return contact_list
        except NoneType:
            print("File {0} prodably is blank".format(self._file_path))
            sys.exit(1)

    def parse_to_csv(self, contact_list):
        file = self._open_file_to_write()
        try:
            strr = ";".join(Contact.ALL_CONTACT_PROPERTIES)
            file.write(str(strr + "\n"))
            for contact in contact_list:
                contact_parsed_line = ''
                for prop in Contact.ALL_CONTACT_PROPERTIES:
                    try:
                        val = contact.__dict__[prop]
                    except:
                        val = ''
                    if not contact_parsed_line:
                        if not val:
                            val = ' '
                        contact_parsed_line = val
                        continue
                    if isinstance(val, int):
                        val = str(val)
                    if isinstance(val, list):
                        val = ",".join(val)
                    contact_parsed_line = ';'.join([contact_parsed_line, val])
                file.write(str("{0}\n".format(contact_parsed_line.encode('utf-8'))))
            print "Write to file OK."
        except Exception, e:
            print "Some error in parse_to_scv: {0}".format(e)
            sys.exit(1)