# -*- coding: UTF-8 -*-

from log import Log, ErrorLogPathNotExist
from datetime import datetime
from ..command import CommandResources


class LogCommand(Log):
    def __init__(self, **kwargs):
        super(LogCommand, self).__init__(**kwargs)

    def put_log_list_all_command(self, data_to_put):
        self._put_log("[List All Commands] [{0}]".format(datetime.now()))
        for o in data_to_put:
            o = CommandResources(**o)
            self._put_log("\t Command Name: {0}".format(o.name))
        self._put_log("# Count: [{0}]".format(len(data_to_put)))
    
    def put_log_command(self, data_to_put):
        self._put_log("[Get Command] [{0}]".format(datetime.now()))
        data = "\t Command Name: {0} \n" \
                "\t Register: {1} \n" \
                "\t File ID: {2} \n" \
                "\t Command Line: {3}".format(data_to_put.command_name, 
                                     data_to_put.register, 
                                     data_to_put.file_id, 
                                     data_to_put.command_line)
        self._put_log(data)
    
    def put_log_add_command(self, data_to_put):
        data = "\t + [OK] Command Name: {0}".format(data_to_put.command_name)
        self._put_log(data)
    
    def put_log_clone_command(self, data_to_put):
        data = "\t * [OK] Command Name: {0}".format(data_to_put.command_name)
        self._put_log(data)
    
    def put_log_update_command(self, data_to_put):
        data = "\t # [OK] Command Name: {0}".format(data_to_put.command_name)
        self._put_log(data)

    def put_log_delete(self, data_to_put):
        data = "\t - [OK] Command Name: {0}".format(data_to_put.split(' ')[1])
        self._put_log(data)

    def put_error_log_delete_command(self, command, data_to_put):
        data = "\t ** [ERROR] Command Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(command, data_to_put['error'], data_to_put['full_error'])
        self._put_log(data)

    def put_error_log_get_command(self, command, data_to_put):
        data = "\t ** [ERROR] Command Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(command, data_to_put['error'], data_to_put['full_error'])
        self._put_log(data)

    def _put_log(self, data):   
        if self._to_std_out:
            self._print_to_std_out(data)
        if self._log_path:
            self._save_to_file(data)