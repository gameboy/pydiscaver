# -*- coding: UTF-8 -*-

from datetime import datetime


class ErrorLogPathNotExist(IOError):
    pass


class Log(object):
    def __init__(self, log_path=None, to_std_out=True): #, first_line_info=None):
        self._log_path = log_path
        self._to_std_out = to_std_out
        # self._first_line_info = first_line_info

    def put_header_to_log(self, header):
        data = "[{0}] [{1}]".format(header, datetime.now())
        self.put_log(data)

    def put_foot_to_log(self, foot):
        self.put_log(foot)

    def _save_to_file(self, data_to_save):
        try:
            des = open(self._log_path, "a")
            des.write(str(data_to_save))
            des.write("\n")
            des.close()
        except Exception, e:
            raise ErrorLogPathNotExist("{0}".format(e[1]))

    def _print_to_std_out(self, data_to_print):
        print(str(data_to_print))

    def put_log(self, data_to_put):
        if self._to_std_out:
            self._print_to_std_out(data_to_put)
        if self._log_path:
            self._save_to_file(data_to_put)
