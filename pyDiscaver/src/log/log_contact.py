# -*- coding: UTF-8 -*-

from log import Log
from datetime import datetime
from ..contact import ContactResources


class LogContact(Log):
    def put_log_contact(self, data_to_put):
        self.put_log("[Get Contact] [{0}]".format(datetime.now()))
        data = ""
        for key in data_to_put.__dict__:
            if key in data_to_put.ALL_CONTACT_PROPERTIES:
                data += "\t{0}: {1}\n".format(key, data_to_put.__dict__[key])
        self.put_log(data)

    def put_log_list_all_contacts(self, data_to_put):
        self.put_log("[List All Contact] [{0}]".format(datetime.now()))
        for o in data_to_put:
            o = ContactResources(**o)
            self.put_log("\t Contact Name: {0}".format(o.name))
        self.put_log("# Count: [{0}]".format(len(data_to_put)))

    def put_log_add_contact(self, data_to_put):
        data = "\t + [OK] Contact Name: {0}".format(data_to_put.contact_name)
        self.put_log(data)

    def put_log_delete(self, data_to_put):
        data = "\t - [OK] Contact Name: {0}".format(data_to_put.split(' ')[1])
        self.put_log(data)

    def put_log_update_contact(self, data_to_put):
        data = "\t # [OK] Contacts Name: {0}".format(data_to_put.contact_name)
        self.put_log(data)

    def put_log_clone_contact(self, data_to_put):
        data = "\t * [OK] Contacts Name: {0}".format(data_to_put.contact_name)
        self.put_log(data)

    def put_error_log_delete_contact(self, contact, data_to_put):
        data = "\t ** [ERROR] Contact Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(contact, data_to_put['error'], data_to_put['full_error'])
        self.put_log(data)

    def put_error_log_get_contact(self, contact, data_to_put):
        data = "\t ** [ERROR] Contact Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(contact, data_to_put['error'], data_to_put['full_error'])
        self.put_log(data)