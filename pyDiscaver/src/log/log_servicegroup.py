# -*- coding: UTF-8 -*-

from log import Log
from datetime import datetime
from ..servicegroup import ServiceGroupResources


class LogServiceGroup(Log):
    def put_log_servicegroup(self, data_to_put):
        self.put_log("[Get Service Group] [{0}]".format(datetime.now()))
        data = ""
        for key in data_to_put.__dict__:
            if key in data_to_put.ALL_SERVICEGROUP_PROPERTIES:
                data += "\t{0}: {1}\n".format(key, data_to_put.__dict__[key])
        data += "\tService Group Members: \n"
        for service in data_to_put.members['members']:
            data += "\t\t {0} \n".format(service)
        self.put_log(data)

    def put_log_list_all_servicegroup(self, data_to_put):
        self.put_log("[List All Service Group] [{0}]".format(datetime.now()))
        for o in data_to_put:
            o = ServiceGroupResources(**o)
            self.put_log("\t Service Group Name: {0}".format(o.name))
        self.put_log("# Count: [{0}]".format(len(data_to_put)))

    def put_log_add_servicegroup(self, data_to_put):
        data = "\t + [OK] Service Group Name: {0}".format(data_to_put.servicegroup_name)
        self.put_log(data)

    def put_log_delete(self, data_to_put):
        data = "\t - [OK] Service Group Name: {0}".format(data_to_put.split(' ')[1])
        self.put_log(data)

    def put_log_update_servicegroup(self, data_to_put):
        data = "\t # [OK] Service Group Name: {0}".format(data_to_put.servicegroup_name)
        self.put_log(data)

    def put_log_clone_servicegroup(self, data_to_put):
        data = "\t * [OK] Service Group Name: {0}".format(data_to_put.servicegroup_name)
        self.put_log(data)

    def put_error_log_delete_servicegroup(self, servicegroup, data_to_put):
        data = "\t ** [ERROR] Service Group Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(servicegroup, data_to_put['error'], data_to_put['full_error'])
        self.put_log(data)

    def put_error_log_get_servicegroup(self, servicegroup, data_to_put):
        data = "\t ** [ERROR] Service Group Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(servicegroup, data_to_put['error'], data_to_put['full_error'])
        self.put_log(data)