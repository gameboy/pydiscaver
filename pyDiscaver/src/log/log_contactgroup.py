# -*- coding: UTF-8 -*-
from log import Log
from datetime import datetime
from ..contactgroup import ContactGroupResources


class LogContactGroup(Log):

    def put_log_contactgroup(self, data_to_put):
        self.put_log("[Get Contact Group] [{0}]".format(datetime.now()))
        data = ""
        for key in data_to_put.__dict__:
            if key in data_to_put.ALL_CONTACT_GROUP_PROPERTIES:
                data += "\t{0}: {1}\n".format(key, data_to_put.__dict__[key])
        data += "\tContact Group Members:\n"
        for contact in data_to_put.contactgroup_members['members']:
            data += "\t\t {0} \n".format(contact)
        self.put_log(data)

    def put_log_list_all_contactgroup(self, data_to_put):
        self.put_log("[List All Contact Group] [{0}]".format(datetime.now()))
        for o in data_to_put:
            o = ContactGroupResources(**o)
            self.put_log("\t Contact Group Name: {0}".format(o.name))
        self.put_log("# Count: [{0}]".format(len(data_to_put)))

    def put_log_add_contactgroup(self, data_to_put):
        data = "\t + [OK] Contact Group Name: {0}".format(data_to_put.contactgroup_name)
        self.put_log(data)

    def put_log_clone_contactgroup(self, data_to_put):
        data = "\t * [OK] Contact Group Name: {0}".format(data_to_put.contactgroup_name)
        self.put_log(data)

    def put_log_delete(self, data_to_put):
        data = "\t - [OK] Contact Group Name: {0}".format(data_to_put.split(' ')[1])
        self.put_log(data)

    def put_log_update_contactgroup(self, data_to_put):
        data = "\t # [OK] Contact Group Name: {0}".format(data_to_put.contactgroup_name)
        self.put_log(data)

    def put_error_log_delete_contactgroup(self, contactgroup, data_to_put):
        data = "\t ** [ERROR] Contact Group Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(contactgroup, data_to_put['error'], data_to_put['full_error'])
        self.put_log(data)

    def put_error_log_get_contactgroup(self, contactgroup, data_to_put):
        data = "\t ** [ERROR] Contact Group Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(contactgroup, data_to_put['error'], data_to_put['full_error'])
        self.put_log(data)

    def put_error_log_add_contactgroup(self, contactgroup, data_to_put):
        data = "\t ** [ERROR] Contact Group Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(contactgroup, data_to_put['error'], data_to_put['full_error'])
        self.put_log(data)