# -*- coding: UTF-8 -*-

from log import Log
from datetime import datetime
from ..hostgroup import HostGroupResources


class LogHostGroup(Log):

    def put_log_hostgroup(self, data_to_put):
        self.put_log("[Get Host Group] [{0}]".format(datetime.now()))
        data = ""
        for key in data_to_put.__dict__:
            if key in data_to_put.ALL_HOST_GROUP_PROPERTIES:
                data += "\t{0}: {1}\n".format(key, data_to_put.__dict__[key])
        data += "\tHost Group Members:\n"
        for host in data_to_put.hostgroup_members['members']:
            data += "\t\t {0} \n".format(host)
        self.put_log(data)

    def put_log_clone_hostgroup(self, data_to_put):
        data = "\t * [OK] Host Group Name: {0}".format(data_to_put.hostgroup_name)
        self.put_log(data)

    def put_log_delete(self, data_to_put):
        data = "\t - [OK] Host Group Name: {0}".format(data_to_put.split(' ')[1])
        self.put_log(data)

    def put_log_add_hostgroup(self, data_to_put):
        data = "\t + [OK] Host Group Name: {0}".format(data_to_put.hostgroup_name)
        self.put_log(data)

    def put_log_update_hostgroup(self, data_to_put):
        data = "\t # [OK] Host Group Name: {0}".format(data_to_put.hostgroup_name)
        self.put_log(data)

    def put_log_list_all_hostgroup(self, data_to_put):
        self.put_log("[List All Host Group] [{0}]".format(datetime.now()))
        for o in data_to_put:
            o = HostGroupResources(**o)
            self.put_log("\t Host Group Name: {0}".format(o.name))
        self.put_log("# Count: [{0}]".format(len(data_to_put)))

    def put_error_log_delete_hostgroup(self, hostgroup, data_to_put):
        data = "\t ** [ERROR] Host Group Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(hostgroup, data_to_put['error'], data_to_put['full_error'])
        self.put_log(data)

    def put_error_log_get_hostgroup(self, hostgroup, data_to_put):
        data = "\t ** [ERROR] Host Group Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(hostgroup, data_to_put['error'], data_to_put['full_error'])
        self.put_log(data)