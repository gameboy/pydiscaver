from log import Log, ErrorLogPathNotExist
from datetime import datetime
from ..service import ServiceResources


class SOMEERROR(Exception):
    pass


class LogService(Log):
    def __init__(self, **kwargs):
        super(LogService, self).__init__(**kwargs)

    def put_log_list_all_services(self, data_to_put):
        self._put_log("[List All Services] [{0}]".format(datetime.now()))
        for o in data_to_put:
            o = ServiceResources(**o)
            self._put_log("\t Service Name: {0}".format(o.name))
        self._put_log("# Count: [{0}]".format(len(data_to_put)))

    def put_log_clone_service(self, data_to_put):
        data = "\t * [OK] Service Name: {0};{1}".format(data_to_put.host_name, data_to_put.service_description)
        self._put_log(data)

    def put_log_clone_host_service(self, data_to_put, hostname):
        data = "\t * [OK] Service Name: {0};{1}".format(hostname, data_to_put.service_description)
        self._put_log(data)

    def put_log_add_service(self, data_to_put):
        data = "\t + [OK] Service Name: {0};{1}".format(data_to_put.host_name, data_to_put.service_description)
        self._put_log(data)

    def put_log_delete(self, service_name):
        data = "\t - [OK] Service Name: {0}".format(service_name)
        self._put_log(data)

    def put_log_update_service(self, data_to_put):
        data = "\t # [OK] Service Name: {0};{1}".format(data_to_put.host_name, data_to_put.service_description)
        self._put_log(data)

    def put_log_service(self, data_to_put):
        self._put_log("[Get Service] [{0}]".format(datetime.now()))
        data = ""
        for key in data_to_put.__dict__:
            if key in data_to_put.ALL_SERVICE_PROPERTIES:
                data += "\t{0}: {1}\n".format(key, data_to_put.__dict__[key])
        self._put_log(data)

    def put_error_log_add_service(self, service, data_to_put):
        data = "\t ** [ERROR] Service Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(service.service_description, data_to_put['error'], data_to_put['full_error'])
        self._put_log(data)

    def put_error_log_get_service(self, service, data_to_put):
        data = "\t ** [ERROR] Service Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(service, data_to_put['error'], data_to_put['full_error'])
        self._put_log(data)

    def put_error_log_delete_service(self, service, data_to_put):
        data = "\t ** [ERROR] Service Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(service, data_to_put['error'], data_to_put['full_error'])
        self._put_log(data)

    def put_error_log_update_service(self, service, data_to_put):
        data = "\t ** [ERROR] Service Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(service.service_description, data_to_put['error'], data_to_put['full_error'])
        self._put_log(data)

    def put_error_log_clone_service(self, service, data_to_put):
        data = "\t ** [ERROR] Service Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(service, data_to_put['error'], data_to_put['full_error'])
        self._put_log(data)

    def _put_log(self, data):
        if self._to_std_out:
            self._print_to_std_out(data)
        if self._log_path:
            self._save_to_file(data)
