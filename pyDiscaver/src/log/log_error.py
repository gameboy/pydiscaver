# -*- coding: UTF-8 -*-

import log

class LogError(log.Log):
    def __init__(self, **kwargs):
        super(LogError, self).__init__(**kwargs)