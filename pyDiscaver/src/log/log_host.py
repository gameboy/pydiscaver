# -*- coding: UTF-8 -*-

from log import Log, ErrorLogPathNotExist
from datetime import datetime
from ..host import HostResources


class LogHost(Log):
    def __init__(self, **kwargs):
        super(LogHost, self).__init__(**kwargs)

    def put_log_list_all_hosts(self, data_to_put):
        self._put_log("[List All Hosts] [{0}]".format(datetime.now()))
        for o in data_to_put:
            o = HostResources(**o)
            self._put_log("\t Host Name: {0}".format(o.name))
        self._put_log("# Count: [{0}]".format(len(data_to_put)))
    
    def put_log_host(self, data_to_put):
        self._put_log("[Get Host] [{0}]".format(datetime.now()))
        data = ""
        for key in data_to_put.__dict__:
            if key in data_to_put.ALL_HOST_PROPERTIES:
                data += "\t{0}: {1}\n".format(key, data_to_put.__dict__[key])
        self._put_log(data)

    def put_log_add_host(self, data_to_put):
        data = "\t + [OK] Host Name: {0}".format(data_to_put.host_name)
        self._put_log(data)
    
    def put_log_delete(self, data_to_put):
        data = "\t - [OK] Host Name: {0}".format(data_to_put.split(' ')[1])
        self._put_log(data)

    def put_log_update_host(self, data_to_put):
        data = "\t # [OK] Host Name: {0}".format(data_to_put.host_name)
        self._put_log(data)

    def put_log_clone_host(self, data_to_put):
        data = "\t * [OK] Host Name: {0}".format(data_to_put.host_name)
        self._put_log(data)

    def put_error_log_add_host(self, host, data_to_put):
        data = "\t ** [ERROR] Host Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(host.host_name, data_to_put['error'], data_to_put['full_error'])
        self._put_log(data)

    def put_error_log_update_host(self, host, data_to_put):
        data = "\t ** [ERROR] Host Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(host.host_name, data_to_put['error'], data_to_put['full_error'])
        self._put_log(data)

    def put_error_log_delete_host(self, host_name, data_to_put):
        data = "\t ** [ERROR] Host Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(host_name, data_to_put['error'], data_to_put['full_error'])
        self._put_log(data)

    def put_error_log_get_host(self, host_name, data_to_put):
        data = "\t ** [ERROR] Host Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(host_name, data_to_put['error'], data_to_put['full_error'])
        self._put_log(data)

    def put_error_log_clone_host(self, host_name, data_to_put):
        data = "\t ** [ERROR] Host Name: {0}, \n\t\tError: {1}, \n\t\tFull Error: {2}".format(host_name, data_to_put['error'], data_to_put['full_error'])
        self._put_log(data)

    def _put_log(self, data):   
        if self._to_std_out:
            self._print_to_std_out(data)
        if self._log_path:
            self._save_to_file(data) 