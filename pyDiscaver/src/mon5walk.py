# -*- coding: UTF-8 -*-
import sys
from mon5walkOptions import Mon5WalkOptions
from log import *
from monitors import *
from parsers.command_parser import ParserCommandCSV
from parsers.host_parser import ParserHostCSV
from parsers.service_parser import ParserServiceCSV
from parsers.hostgroup_parser import ParserHostGroupCSV
from parsers.servicegroup_parser import ParserServiceGroupCSV
from parsers.contact_parser import ParserContactCSV
from parsers.contactgroup_parser import ParserContactGroupCSV
from parsers.host_parser_ing import ParserHostINGCSV
from parsers.service_parser_ing import ParserServiceINGCSV


class Mon5Walk(object):
    def __init__(self, mon5walkoptions):
        self.options = mon5walkoptions

    def run(self):
        if self.options.type == 'config':
            if self.options.object == 'command':
                Mon5WalkCommand(self.options)
            elif self.options.object == 'host':
                Mon5WalkHost(self.options)
            elif self.options.object == 'service':
                Mon5WalkService(self.options)
            elif self.options.object == 'contact-group':
                Mon5WalkContactGroup(self.options)
            elif self.options.object == 'service-group':
                Mon5WalkServiceGroup(self.options)
            elif self.options.object == 'host-group':
                Mon5WalkHostGroup(self.options)
            elif self.options.object == "contact":
                Mon5WalkContact(self.options)
            else:
                print "No't implemented, see manual"
        elif self.options.type == 'status':
            print "Akcje dla statusu"
        else:
            print "No't implemented, see manual"

        sys.exit(0)



class Mon5WalkCommand(object):
    def __init__(self, options):
        self.log = LogCommand(log_path=options.log_output, to_std_out=options.no_log_screen)
        self.mon = monitor_command.MonitorCommand(user=options.username,
                                                  password=options.password,
                                                  address=options.server,
                                                  log=self.log)
        action = options.action
        if action == 'list':
            self.mon.list_all()
        if action == 'get':
            for a in options.args:
                self.mon.get_command(a)
        if action == 'import':
            for a in options.args:
                parser = ParserCommandCSV(file_path=a, split_char=';')
                p = parser.parse()
                self.mon.add_commands(p)
        if action == 'add':
            for a in options.args:
                parser = ParserCommandCSV(file_path=None, split_char=';')
                p = parser.parse_cli_command_data(a)
                self.mon.add_commands(p)
        if action == 'update':
            for a in options.args:
                parser = ParserCommandCSV(file_path=a, split_char=';')
                p = parser.parse()
                self.mon.update_commands(p)
        if action == 'delete':
            for a in options.args:
                self.mon.delete(a)
        if action == 'clone':
            parser = ParserCommandCSV(file_path=options.args[0], split_char=';')
            command_list = self.mon.clone()
            parser.parse_to_csv(command_list)
        if options.save:
            self.mon.save_changes()


class Mon5WalkHost(object):
    def __init__(self, options):
        self.log = LogHost(log_path=options.log_output, to_std_out=options.no_log_screen)
        self.mon = monitor_host.MonitorHost(user=options.username,
                                            password=options.password,
                                            address=options.server,
                                            log=self.log,
                                            options=options)
        action = options.action
        if action == 'list':
            self.mon.list_all()
        if action == 'get':
            for a in options.args:
                self.mon.get_host(a)
        if action == 'add':
            for a in options.args:
                parser = ParserHostCSV(file_path=None, split_char=';')
                p = parser.parse_cli_host_data(a)
                self.mon.add_hosts(p)
        if action == 'import':
            for a in options.args:
                parser = ParserHostCSV(file_path=a, split_char=';')
                p = parser.parse()
                self.mon.add_hosts(p)
        if action == 'update':
            for a in options.args:
                parser = ParserHostCSV(file_path=a, split_char=';')
                p = parser.parse()
                self.mon.update_hosts(p)
        if action == 'delete':
            for a in options.args:
                self.mon.delete(a)
                if options.delete_rdd_xml:
                    import os
                    if not os.path.exists("/usr/bin/mon"):
                        print "mon command not find. Are you on server ?"
                        sys.exit(1)
                    from subprocess import Popen, PIPE
                    cmd = 'mon node ctrl --self -all `rm -Rf  /opt/monitor/op5/pnp/perfdata/{0}`'.format(a)
                    p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
                    out, err = p.communicate()
                    if p.returncode:
                        print "Cant delete RRD and XML: {0}".format(err)
                        continue
                    print "Delete RRD and XML folder"
        if action == 'clone':
            parser = ParserHostCSV(file_path=options.args[0], split_char=';')
            if options.host_to_clone_service:
                hosts_list = self.mon.clone_host(options.host_to_clone_service)
            elif options.hostgroup_name:
                hosts_list = self.mon.clone_hostgroup(options.hostgroup_name)
            elif options.unavailable_from:
                hosts_list = self.mon.clone_unavailable_from(options.unavailable_from)
            else:
                hosts_list = self.mon.clone()
            if options.with_service:
                options.host_list = hosts_list.get_host_list()
                Mon5WalkService(options)
            parser.parse_to_csv(hosts_list)
        if options.save:
                self.mon.save_changes()


class Mon5WalkService(object):
    def __init__(self, options):
        self.log = LogService(log_path=options.log_output, to_std_out=options.no_log_screen)
        self.mon = monitor_service.MonitorService(user=options.username,
                                                  password=options.password,
                                                  address=options.server,
                                                  log=self.log,
                                                  options=options)
        action = options.action
        if action == 'list':
            self.mon.list_all()
        if action == 'get':
            for a in options.args:
                self.mon.get_service(a)
        if action == 'add':
            for a in options.args:
                parser = ParserServiceCSV(file_path=None, split_char=';')
                p = parser.parse_cli_service_data(a)
                self.mon.add_service(p)
        if action == 'import':
            for a in options.args:
                parser = ParserServiceCSV(file_path=a, split_char=';')
                p = parser.parse()
                self.mon.add_services(p)
        if action == 'update':
            for a in options.args:
                parser = ParserServiceCSV(file_path=a, split_char=';')
                p = parser.parse()
                self.mon.update_services(p)
        if action == 'delete':
            for a in options.args:
                self.mon.delete(a)
                if options.delete_rdd_xml:
                    import os
                    if not os.path.exists("/usr/bin/mon"):
                        print "mon command not find. Are you on server ?"
                        sys.exit(1)
                    from subprocess import Popen, PIPE
                    host_name, service_name = a.split(';')
                    cmd = 'mon node ctrl --self -all `rm -Rf  /opt/monitor/op5/pnp/perfdata/{0}/{1}.rrd`'.format(host_name, service_name)
                    cmd1 = 'mon node ctrl --self -all `rm -Rf  /opt/monitor/op5/pnp/perfdata/{0}/{1}.xml`'.format(host_name, service_name)
                    p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
                    q = Popen(cmd1, shell=True, stdout=PIPE, stderr=PIPE)
                    out, err = p.communicate()
                    out1, err1 = q.communicate()
                    if p.returncode or q.returncode:
                        print "Cant delete XML and RRD: {0}".format(err)
                        continue
                    print "Delete RDD and XML folder"
        if action == 'clone':
            if options.with_service:
                aa = options.args[0].split('.')
                file_name = "{0}_services.{1}".format(aa[0], aa[1])
            else:
                file_name = "{0}".format(options.args[0])
            parser = ParserServiceCSV(file_path=file_name, split_char=';')
            if options.host_to_clone_service:
                services_list = self.mon.clone_host_services(options.host_to_clone_service)
            elif options.service_name_to_clone:
                services_list = self.mon.clone_service_by_name(options.service_name_to_clone)
            elif options.service_group_name:
                services_list = self.mon.clone_servicegroup_service(options.service_group_name)
            else:
                services_list = self.mon.clone()
            parser.parse_to_csv(services_list)
        if options.save:
            self.mon.save_changes()


class Mon5WalkHostGroup(object):
    def __init__(self, options):
        self.log = LogHostGroup(log_path=options.log_output, to_std_out=options.no_log_screen)
        self.mon = monitor_hostgroup.MonitorHostGroup(user=options.username,
                                                      password=options.password,
                                                      address=options.server,
                                                      log=self.log)
        action = options.action
        if action == 'list':
            self.mon.list_all()
        if action == 'get':
            for a in options.args:
                self.mon.get_hostgroup(a)
        if action == 'add':
            for a in options.args:
                parser = ParserHostGroupCSV(file_path=None, split_char=';')
                p = parser.parse_cli_command_data(a)
                self.mon.add_hostgroups(p)
        if action == 'delete':
            for a in options.args:
                self.mon.delete(a)
        if action == 'import':
            for a in options.args:
                parser = ParserHostGroupCSV(file_path=a, split_char=';')
                p = parser.parse()
                self.mon.add_hostgroups(p)
        if action == 'update':
            for a in options.args:
                parser = ParserHostGroupCSV(file_path=a, split_char=';')
                p = parser.parse()
                self.mon.add_hostgroups(p)
        if action == 'clone':
            parser = ParserHostGroupCSV(file_path=options.args[0], split_char=';')
            hostgroup_list = self.mon.clone()
            parser.parse_to_csv(hostgroup_list)
        if options.save:
            self.mon.save_changes()


class Mon5WalkServiceGroup(object):
    def __init__(self, options):
        self.log = LogServiceGroup(log_path=options.log_output, to_std_out=options.no_log_screen)
        self.mon = monitor_servicegroup.MonitorServiceGroup(user=options.username,
                                                            password=options.password,
                                                            address=options.server,
                                                            log=self.log)
        action = options.action
        if action == 'list':
            self.mon.list_all()
        if action == 'get':
            for a in options.args:
                self.mon.get_servicegroup(a)
        if action == 'add':
            for a in options.args:
                parser = ParserServiceGroupCSV(file_path=None, split_char=';')
                p = parser.parse_cli_command_data(a)
                self.mon.add_servicegroups(p)
        if action == 'import':
            for a in options.args:
                parser = ParserServiceGroupCSV(file_path=a, split_char=';')
                p = parser.parse()
                self.mon.add_servicegroups(p)
        if action == 'update':
            for a in options.args:
                parser = ParserServiceGroupCSV(file_path=a, split_char=';')
                p = parser.parse()
                self.mon.add_servicegroups(p)
        if action == 'delete':
            for a in options.args:
                self.mon.delete(a)
        if action == 'clone':
            parser = ParserServiceGroupCSV(file_path=options.args[0], split_char=';')
            service_group = self.mon.clone()
            parser.parse_to_csv(service_group)
        if options.save:
            self.mon.save_changes()


class Mon5WalkContact(object):
    def __init__(self, options):
        self.log = LogContact(log_path=options.log_output, to_std_out=options.no_log_screen)
        self.mon = monitor_contact.MonitorContact(user=options.username,
                                                  password=options.password,
                                                  address=options.server,
                                                  log=self.log)
        action = options.action
        if action == 'list':
            self.mon.list_all()
        if action == 'get':
            for a in options.args:
                self.mon.get_contact(a)
        if action == 'add':
            for a in options.args:
                parser = ParserContactCSV(file_path=None, split_char=';')
                p = parser.parse_cli_contact_data(a)
                self.mon.add_contacts(p)
        if action == 'import':
            for a in options.args:
                parser = ParserContactCSV(file_path=a, split_char=';')
                p = parser.parse()
                #print p[0].contact_name
                #sys.exit(0)
                self.mon.add_contacts(p)
        if action == 'update':
            for a in options.args:
                parser = ParserContactCSV(file_path=a, split_char=';')
                p = parser.parse()
                self.mon.add_contacts(p)
        if action == 'delete':
            for a in options.args:
                self.mon.delete(a)
        if action == 'clone':
            parser = ParserContactCSV(file_path=options.args[0], split_char=';')
            contact_list = self.mon.clone()
            parser.parse_to_csv(contact_list)
        if options.save:
            self.mon.save_changes()


class Mon5WalkContactGroup(object):
    def __init__(self, options):
        self.log = LogContactGroup(log_path=options.log_output, to_std_out=options.no_log_screen)
        self.mon = monitor_contactgroup.MonitorContactGroup(user=options.username,
                                                            password=options.password,
                                                            address=options.server,
                                                            log=self.log)
        action = options.action
        if action == 'list':
            self.mon.list_all()
        if action == 'get':
            for a in options.args:
                self.mon.get_contactgroup(a)
        if action == 'add':
            for a in options.args:
                parser = ParserContactGroupCSV(file_path=None, split_char=';')
                p = parser.parse_cli_command_data(a)
                self.mon.add_contactgroup(p)
        if action == 'import':
            for a in options.args:
                parser = ParserContactGroupCSV(file_path=a, split_char=';')
                p = parser.parse()
                self.mon.add_contactgroups(p)
        if action == 'delete':
            for a in options.args:
                self.mon.delete(a)
        if action == 'clone':
            parser = ParserContactGroupCSV(file_path=options.args[0], split_char=';')
            contactgroup_list = self.mon.clone()
            parser.parse_to_csv(contactgroup_list)
        if options.save:
            self.mon.save_changes()


class Mon5WalkING(object):
    def __init__(self, option, options):
        self.log_host = LogHost(log_path=options.log_output, to_std_out=options.no_log_screen)
        self.log_service = LogService(log_path=options.log_output, to_std_out=options.no_log_screen)
        self.mon_host = monitor_host.MonitorHost(user=options.username,
                                                 password=options.password,
                                                 address=options.server,
                                                 log=self.log_host)
        self.mon_service = monitor_service.MonitorService(user=options.username,
                                                          password=options.password,
                                                          address=options.server,
                                                          log=self.log_service)
        (key, value) = option.items()[0]
        if key == 'add_host_ing':
            parser = ParserHostINGCSV(file_path=value, split_char=';')
            p = parser.parse()
            self.mon_host.add_hosts(p)
            if options.save:
                self.mon_host.save_changes()

        if key == 'add_services_ing':
            parser = ParserServiceINGCSV(file_path=value, split_char=';')
            p = parser.parse()
            self.mon_service.add_services(p)
            if options.save:
                self.mon_service.save_changes()