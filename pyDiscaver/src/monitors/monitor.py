# -*- coding: UTF-8 -*-
import requests


class Monitor(object):
    def __init__(self, address, user, password, no_save=False, log=None):
        self.address = address
        self.user = user
        self.password = password
        self.request = "{0}/api/config".format(self.address)
        self.status = "{0}/api/status".format(self.address)
        self.auth = (self.user, self.password)
        self._log = log
        self.no_save = no_save

    def save_changes(self):
        r = requests.post('{0}/change'.format(self.request), auth=self.auth, verify=False)
        if r.status_code == 500:
            print "\t ------------------------------"
            print "\t| Save Status: Nothing to save |"
            print "\t ------------------------------"
        if r.status_code == 200:
            print "\t ---------------------------"
            print "\t| Save Status: Changes save |"
            print "\t ---------------------------"

    def _check_error_data(self, data):
        try:
            error = data['error']
            return True
        except Exception:
            return False



