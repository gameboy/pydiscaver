# -*- coding: UTF-8 -*-

import json

import requests
from requests.exceptions import ConnectionError

from ..servicegroup import ServiceGroupList, ServiceGroup
from ..log import log_error
import monitor


class MonitorServiceGroup(monitor.Monitor):
    def __init__(self, **kwargs):
        super(MonitorServiceGroup, self).__init__(**kwargs)

    def get_servicegroup(self, servicegroup_name):
        try:
            r =requests.get('{0}/servicegroup/{1}?format=json'.format(self.request, servicegroup_name),
                            auth=self.auth,
                            verify=False)
            data = r.text
        except ConnectionError:
            data = json.dumps({'error': 'Connection Error', 'full_error': 'Connection time out'})
        output = json.loads(data)
        if self._check_error_data(output):
            self._log.put_error_log_get_servicegroup(servicegroup_name, output)
        else:
            self._log.put_log_servicegroup(ServiceGroup(**output))

    def list_all(self):
        try:
            r = requests.get('{0}/servicegroup?format=json'.format(self.request),
                             auth=self.auth,
                             verify=False)
            data = r.text
        except ConnectionError:
            data = json.dumps({'error': 'Connection Error', 'full_error': 'Connection time out'})
        output = json.loads(data)
        if self._check_error_data(output):
            log_error.LogError(log_path=self._log._log_path,
                               to_std_out=self._log._to_std_out).put_log(output)
        else:
            self._log.put_log_list_all_servicegroup(output)

    def add_servicegroups(self, servicegroups):
        if not isinstance(servicegroups, list):
            servicegroups = [servicegroups]
        self._log.put_header_to_log("Add Service Groups")
        for servicegroup in servicegroups:
            output = self.add_servicegroup(servicegroup)

    def add_servicegroup(self, servicegroup):
        if not isinstance(servicegroup, ServiceGroup):
            raise TypeError('item is not of type {0}'.format(servicegroup.__name__))
        data = servicegroup.create_json()
        try:
            r =requests.post('{0}/servicegroup'.format(self.request),
                             auth=self.auth,
                             verify=False,
                             data=data,
                             headers={'content-type': 'application/json'})
            data = r.text
        except ConnectionError:
            data = json.dumps({'error': 'ConnectionError', 'full_error': 'Connection time out'})
        output = json.loads(data)
        if self._check_error_data(output):
            log_error.LogError(log_path=self._log._log_path,
                               to_std_out=self._log._to_std_out).put_log(output)
        else:
            self._log.put_log_add_servicegroup(ServiceGroup(**output))
        return output

    def delete(self, servicegroup_name):
        try:
            r = requests.delete('{0}/servicegroup/{1}?format=json'.format(self.request, servicegroup_name),
                            auth=self.auth,
                            verify=False)
            data = json.loads(r.text)
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        self._log.put_header_to_log("Delete Service Group")
        if self._check_error_data(data):
            self._log.put_error_log_delete_servicegroup(servicegroup_name, data)
        else:
            self._log.put_log_delete(data)

    def update_servicegroups(self, servicegroups):
        self._log.put_header_to_log("Update Service Groups")
        for servicegroup in servicegroups:
            data = self.update_servicegroup(servicegroup)
            if self._check_error_data(data):
                log_error.LogError(log_path=self._log._log_path,
                                   to_std_out=self._log._to_std_out,
                                   first_line_info="Update Service Group Error").put_log(data)
            else:
                self._log.put_log_update_servicegroup(ServiceGroup(**data))

    def update_servicegroup(self, servicegroup):
        if not isinstance(servicegroup, ServiceGroup):
            raise TypeError('item is not of type {}'.format(ServiceGroup.__name__))
        data = servicegroup.create_json()
        try:
            r = requests.patch('{0}/servicegroup/{1}'.format(self.request,
                                                             json.loads(data)['servicegroup_name']),
                               auth=self.auth,
                               verify=False,
                               data=data,
                               headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except Exception, e:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out {0}'}

        return data

    def clone(self):
        try:
            r = requests.get('{0}/servicegroup?format=json'.format(self.request),
                         auth=self.auth, verify=False,
                         headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        servicegroup_list = ServiceGroupList()
        self._log.put_header_to_log("Clone Service Groups")
        for servicegroup in data:
            r = requests.get('{0}'.format(servicegroup['resource']),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})
            data1 = json.loads(r.text)
            if self._check_error_data(data1):
                log_error.LogError(log_path=self._log._log_path,
                                   to_std_out=self._log._to_std_out).put_log(data1)
                continue
            else:
                self._log.put_log_clone_servicegroup(ServiceGroup(**data1))
            servicegroup_list.append(ServiceGroup(**data1))
        self._log.put_foot_to_log("#Count: [{0}]".format(len(servicegroup_list)))
        return  servicegroup_list