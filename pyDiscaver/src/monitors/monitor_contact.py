# -*- coding: UTF-8 -*-

import json
import requests
from requests.exceptions import ConnectionError
from ..log import log_error
from ..contact import Contact, ContactList
import monitor


class MonitorContact(monitor.Monitor):
    def list_all(self):
        try:
            r = requests.get('{0}/contact?format=json'.format(self.request), auth=self.auth, verify=False)
            data = r.text
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}

        output = json.loads(data)

        if self._check_error_data(output):
            log_error.LogError(log_path=self._log._log_path, to_std_out=self._log._to_std_out).put_log(output)
        else:
            self._log.put_log_list_all_contacts(output)

    def get_contact(self, name):
        try:
            r = requests.get('{0}/contact/{1}?format=json'.format(self.request, name), auth=self.auth, verify=False)
            data = r.text
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        output = json.loads(data)
        if self._check_error_data(output):
            self._log.put_error_log_get_contact(name, output)
        else:
            self._log.put_log_contact(Contact(**output))

    def add_contacts(self, contacts):
        if not isinstance(contacts, list):
            contacts = [contacts]
        self._log.put_header_to_log("Add Contacts")
        for contact in contacts:
            output = self.add_contact(contact)

    def add_contact(self, contact):
        if not isinstance(contact, Contact):
            raise TypeError('item is not of type {}'.format(Contact.__name__))
        data = contact.create_json()
        try:
            r = requests.post('{0}/contact'.format(self.request),
                              auth=self.auth,
                              verify=False,
                              data=data,
                              headers={'content-type': 'application/json'})
            data = r.text
        except:
            data = json.dumps({'error': 'ConnectionError', 'full_error': 'Connection time out'})
        output = json.loads(data)
        if self._check_error_data(output):
            log_error.LogError(log_path=self._log._log_path,
                               to_std_out=self._log._to_std_out).put_log(output)
        else:
            self._log.put_log_add_contact(Contact(**output))
        return output

    def delete(self, contact_name):
        try:
            r = requests.delete('{0}/contact/{1}?format=json'.format(self.request, contact_name),
                                auth=self.auth,
                                verify=False)
            data = json.loads(r.text)
        except:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        self._log.put_header_to_log("Delete Contact")
        if self._check_error_data(data):
            self._log.put_error_log_delete_contact(contact_name, data)
        else:
            self._log.put_log_delete(data)

    def update_contact(self, contact):
        if not isinstance(contact, Contact):
            raise TypeError('item is not of type {}'.format(Contact.__name__))
        data = contact.create_json()
        try:
            r = requests.patch('{0}/contact/{1}'.format(self.request,
                                                        json.loads(data)['contact_name']),
                                                        auth=self.auth,
                                                        verify=False,
                                                        data=data, headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except Exception, e:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}

        return data

    def update_contacts(self, contacts):
        self._log.put_header_to_log("Update Contacts")
        for contact in contacts:
            data = self.update_contact(contact)
            if self._check_error_data(data):
                log_error.LogError(log_path=self._log._log_path,
                                   to_std_out=self._log._to_std_out).put_log(data)
            else:
                self._log.put_log_update_contact(Contact(**data))

    def clone(self):
        try:
            r = requests.get('{0}/contact?format=json'.format(self.request),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}

        contact_list = ContactList()
        self._log.put_header_to_log("Clone Contact")
        for contact in data:
            r = requests.get('{0}'.format(contact['resource']),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})

            output = json.loads(r.text)
            if self._check_error_data(output):
                log_error.LogError(log_path=self._log._log_path,
                                   to_std_out=self._log._to_std_out).put_log(output)
                continue
            else:
                self._log.put_log_clone_contact(Contact(**output))
            contact_list.append(Contact(**output))
        self._log.put_foot_to_log("# Count: [{0}]".format(len(contact_list)))
        return contact_list