# -*- coding: UTF-8 -*-

import json

import requests
from requests.exceptions import ConnectionError

from ..hostgroup import HostGroup, HostGroupList
from ..log import log_error
import monitor


class MonitorHostGroup(monitor.Monitor):
    def __init__(self, **kwargs):
        super(MonitorHostGroup, self).__init__(**kwargs)

    def get_hostgroup(self, hostgroup_name):
        try:
            r = requests.get('{0}/hostgroup/{1}?format=json'.format(self.request, hostgroup_name),
                             auth=self.auth,
                             verify=False)
            data = r.text
        except ConnectionError:
            data = json.dumps({'error': 'Connection Error', 'full_error': 'Connection time out'})

        output = json.loads(data)

        if self._check_error_data(output):
            self._log.put_error_log_get_hostgroup(hostgroup_name, output)
        else:
            self._log.put_log_hostgroup(HostGroup(**output))

    def list_all(self):
        try:
            r = requests.get('{0}/hostgroup?format=json'.format(self.request), auth=self.auth, verify=False)
            data = r.text
        except ConnectionError:
            data = json.dumps({'error': 'Connection Error', 'full_error': 'Connection time out'})
        output = json.loads(data)
        if self._check_error_data(output):
            log_error.LogError(log_path=self._log._log_path,
                               to_std_out=self._log._to_std_out).put_log(output)
        else:
            self._log.put_log_list_all_hostgroup(output)

    def clone(self):
        try:
            r = requests.get('{0}/hostgroup?format=json'.format(self.request),
                         auth=self.auth, verify=False,
                         headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        hostgroup_list = HostGroupList()
        self._log.put_header_to_log("Clone Host Groups")
        for hostgroup in data:
            r = requests.get('{0}'.format(hostgroup['resource']),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})
            data1 = json.loads(r.text)
            if self._check_error_data(data1):
                log_error.LogError(log_path=self._log._log_path,
                                   to_std_out=self._log._to_std_out).put_log(data1)
                continue
            else:
                self._log.put_log_clone_hostgroup(HostGroup(**data1))
            hostgroup_list.append(HostGroup(**data1))
        self._log.put_foot_to_log("#Count: [{0}]".format(len(hostgroup_list)))
        return  hostgroup_list

    def delete(self, hostgroup_name):
        try:
            r = requests.delete('{0}/hostgroup/{1}?format=json'.format(self.request, hostgroup_name),
                                auth=self.auth,
                                verify=False)
            data = json.loads(r.text)
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        self._log.put_header_to_log("Delete Host Group")
        if self._check_error_data(data):
            self._log.put_error_log_delete_hostgroup(hostgroup_name, data)
        else:
            self._log.put_log_delete(data)

    def add_hostgroup(self, hostgroup):
        if not isinstance(hostgroup, HostGroup):
            raise TypeError('item is not of type {0}'.format(hostgroup.__name__))
        data = hostgroup.create_json()
        try:
            r = requests.post('{0}/hostgroup'.format(self.request),
                              auth=self.auth,
                              verify=False,
                              data=data,
                              headers={'content-type': 'application/json'})
            data = r.text
        except ConnectionError:
            data = json.dumps({'error': 'ConnectionError', 'full_error': 'Connection time out'})
        output = json.loads(data)
        if self._check_error_data(output):
            log_error.LogError(log_path=self._log._log_path,
                               to_std_out=self._log._to_std_out).put_log(output)
        else:
            self._log.put_log_add_hostgroup(HostGroup(**output))
        return output

    def add_hostgroups(self, hostgroups):
        if not isinstance(hostgroups, list):
            hostgroups = [hostgroups]
        self._log.put_header_to_log("Add Host Groups")
        for hostgroup in hostgroups:
            output = self.add_hostgroup(hostgroup)

    def update_hostgroup(self, hostgroup):
        if not isinstance(hostgroup, HostGroup):
            raise TypeError('item is not of type {}'.format(HostGroup.__name__))
        data = hostgroup.create_json()
        try:
            r = requests.patch('{0}/hostgroup/{1}'.format(self.request,
                                                        json.loads(data)['hostgroup_name']),
                                                        auth=self.auth,
                                                        verify=False,
                                                        data=data, headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except Exception, e:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}

        return data

    def update_hostgroups(self, hostgroups):
        self._log.put_header_to_log("Update Host Groups")
        for hostgroup in hostgroups:
            data = self.update_hostgroup(hostgroup)
            if self._check_error_data(data):
                log_error.LogError(log_path=self._log._log_path,
                                   to_std_out=self._log._to_std_out).put_log(data)
            else:
                self._log.put_log_update_hostgroup(HostGroup(**data))