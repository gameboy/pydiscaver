# -*- coding: UTF-8 -*-

import json
import sys
import requests
from requests.exceptions import ConnectionError

from ..host import Host, HostList
from ..log import log_error
import monitor
from ..service import Service
from ..monitors.monitor_service import MonitorService
from ..log.log_service import LogService
from datetime import datetime


class MonitorHost(monitor.Monitor):
    def __init__(self, log_path=None, **kwargs):
        if 'options' in kwargs:
            self.options = kwargs.pop('options')
        super(MonitorHost, self).__init__(**kwargs)

    def add_host(self, host):
        if not isinstance(host, Host):
            raise TypeError('item is not of type {}'.format(Host))
        data = host.create_json()
        try:
            r = requests.post('{0}/host'.format(self.request),
                              auth=self.auth,
                              verify=False,
                              data=data,
                              headers={'content-type': 'application/json'})
            data = r.text
        except:
            data = json.dumps({'error': 'Connection Error', 'full_error': 'Connection time out'})
        output = json.loads(data)
        if self._check_error_data(output):
            self._log.put_error_log_add_host(host, output)
        else:
            self._log.put_log_add_host(Host(**output))
        if self.options.add_ping:
            service_data = {'host_name': host.host_name,
                            'service_description': 'PING',
                            'check_command': 'check_ping',
                            'check_command_args': '100,20%!500,60%'
                            }
            service = Service(**service_data)
            service_log = LogService(log_path=self._log._log_path, to_std_out=self._log._to_std_out)
            service_monitor = MonitorService(address=self.address, user=self.user, password=self.password, no_save=self.no_save, log=service_log)
            service_monitor.add_service(service)
        return output
    
    def add_hosts(self, hosts):
        if not isinstance(hosts, list):
            hosts = [hosts]  
        self._log.put_header_to_log("Add Hosts")
        for host in hosts:
            output = self.add_host(host)

    def list_all(self):
        try:
            r = requests.get('{0}/host?format=json'.format(self.request), auth=self.auth, verify=False)
            data = r.text
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        output = json.loads(data)
        if self._check_error_data(output):
            log_error.LogError(log_path=self._log._log_path,
                               to_std_out=self._log._to_std_out).put_log(output)
        else:
            self._log.put_log_list_all_hosts(output)
                    
    def get_host(self, name):
        try:
            r = requests.get('{0}/host/{1}?format=json'.format(self.request, name),
                             auth=self.auth,
                             verify=False)
            data = r.text
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
            
        output = json.loads(data)
        if self._check_error_data(output):
            self._log.put_error_log_get_host(name, output)
        else:
            self._log.put_log_host(Host(**output))
    
    def host_count(self):
        hosts = self._get_resource_hosts()
        return hosts.__len__()
    
    def update_hosts(self, hosts):
        for host in hosts:
            output = self.update_host(host)
            if self._check_error_data(output):
                self._log.put_error_log_update_host(host, output)
                continue
            else:
                self._log.put_log_update_host(Host(**output))

    def update_host(self, host):
        if not isinstance(host, Host):
            raise TypeError('item is not of type {}'.format(Host))
        data = host.create_json()
        try:
            host_name = json.loads(data).pop('host_name')
        except:
            print "host_name field is required to update host"
            sys.exit(1)
        try:
            r = requests.patch('{0}/host/{1}'.format(self.request, host_name),
                               auth=self.auth,
                               verify=False,
                               data=data, headers={'content-type': 'application/json'})
            data = r.text
        except:
            data = json.dumps({'error': 'Connection Error', 'full_error': 'Connection time out'})
        output = json.loads(data)
        return output

    def delete(self, host_name):
        try:
            r = requests.delete('{0}/host/{1}?format=json'.format(self.request, host_name),
                                auth=self.auth,
                                verify=False)
            data = r.text
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        
        output = json.loads(data)
        self._log.put_header_to_log("Delete Host")
        if self._check_error_data(output):
            self._log.put_error_log_delete_host(host_name, output)
        else:
            self._log.put_log_delete(output)

    def clone_unavailable_from(self, unavailable_from):
        time = unavailable_from.split('+')[0]
        metric = unavailable_from.split('+')[1]
        try:
            r = requests.get('{0}/host?format=json'.format(self.status),
                             auth=self.auth,
                             verify=False)
            data = r.text
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        output = json.loads(data)
        if self._check_error_data(output):
            self._log.put_error_log_clone_host("Hosts", output)
            sys.exit(1)
        host_list = HostList()
        for host in output:
            try:
                r = requests.get('{0}?format=json'.format(host['resource']),
                                 auth=self.auth,
                                 verify=False)
                data = r.text
            except ConnectionError:
                data = {'error': 'Connection Error', 'full_error': 'Connection time out'}

            if self._check_error_data(output):
                self._log.put_error_log_delete_host(host['name'], output)
            else:
                output = json.loads(data)
                if output['last_state'] == 1:
                    try:
                        downtime = output['last_state_change']
                    except:
                        continue
                    total = datetime.today() - datetime.fromtimestamp(downtime)
                    if total.days > int(time):
                        try:
                            r = requests.get('{0}/host/{1}?format=json'.format(self.request, host['name']), auth=self.auth, verify=False)
                            data = r.text
                        except ConnectionError:
                            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
                        output = json.loads(data)
                        if self._check_error_data(output):
                            self._log.put_error_log_delete_host(host['name'], output)
                        else:
                            self._log.put_log_clone_host(Host(**output))
                            host_list.append(Host(**output))
        self._log.put_foot_to_log("# Count: [{0}]".format(len(host_list)))
        return host_list

    def clone_host(self, host_name):
        try:
            r = requests.get('{0}/host/{1}?format=json'.format(self.request, host_name),
                             auth=self.auth,
                             verify=False)
            data = r.text
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        output = json.loads(data)
        host_list = HostList()
        self._log.put_header_to_log("Clone Host")
        if self._check_error_data(output):
            self._log.put_error_log_clone_host(host_name, output)
        else:
            self._log.put_log_clone_host(Host(**output))
            host_list.append(Host(**output))
        return host_list

    def clone_hostgroup(self, hostgroup_name):
        try:
            r = requests.get('{0}/hostgroup/{1}?format=json'.format(self.request, hostgroup_name),
                             auth=self.auth,
                             verify=False)
            data = r.text
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        output = json.loads(data)
        if self._check_error_data(output):
            self._log.put_error_log_clone_host("HostGroup", output)
            sys.exit(1)
        data = output['members']
        host_list = HostList()
        self._log.put_header_to_log("Clone Hosts")
        for host in data:
            r = requests.get('{0}/host/{1}?format=json'.format(self.request, host),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})
            output = json.loads(r.text)
            if self._check_error_data(output):
                self._log.put_error_log_clone_host(host, output)
                continue
            else:
                self._log.put_log_clone_host(Host(**output))
            host_list.append(Host(**output))
        self._log.put_foot_to_log("# Count: [{0}]".format(len(host_list)))
        return host_list

    def clone(self):
        try:
            r = requests.get('{0}/host?format=json'.format(self.request),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        host_list = HostList()
        self._log.put_header_to_log("Clone Hosts")
        for host in data:
            r = requests.get('{0}'.format(host['resource']),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})
            output = json.loads(r.text)
            if self._check_error_data(output):
                self._log.put_error_log_clone_host(host['name'], output)
                continue
            else:
                self._log.put_log_clone_host(Host(**output))
            host_list.append(Host(**output))
        self._log.put_foot_to_log("# Count: [{0}]".format(len(host_list)))
        return host_list