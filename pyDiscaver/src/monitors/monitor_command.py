# -*- coding: UTF-8 -*-

import json

import requests
from requests.exceptions import ConnectionError

from ..command import Command, CommandList
from ..log import log_error
import monitor


class MonitorCommand(monitor.Monitor):
    def __init__(self, **kwargs):
        super(MonitorCommand, self).__init__(**kwargs)
        
    def get_command(self, command_name):
        try:
            r = requests.get('{0}/command/{1}?format=json'.format(self.request, command_name),
                             auth=self.auth,
                             verify=False)
            data = r.text
        except ConnectionError:
            data = json.dumps({'error': 'Connection Error', 'full_error': 'Connection time out'})
            
        output = json.loads(data)
        if self._check_error_data(output):
            self._log.put_error_log_get_command(command_name, output)
        else:
            self._log.put_log_command(Command(**output))

    def list_all(self):
        try:
            r = requests.get('{0}/command?format=json'.format(self.request), auth=self.auth, verify=False)
            data = r.text
        except ConnectionError:
            data = json.dumps({'error': 'Connection Error', 'full_error': 'Connection time out'})
        
        output = json.loads(data)
        if self._check_error_data(output):
            log_error.LogError(log_path=self._log._log_path,
                               to_std_out=self._log._to_std_out).put_log(output)
        else:
            self._log.put_log_list_all_command(output)

    def update_commands(self, commands):
        self._log.put_header_to_log("Update Commands")
        for command in commands:
            data = self.update_command(command)
            if self._check_error_data(data):
                log_error.LogError(log_path=self._log._log_path,
                                   to_std_out=self._log._to_std_out).put_log(data)
            else:
                self._log.put_log_update_command(Command(**data))
                
    def update_command(self, command):
        if not isinstance(command, Command):
            raise TypeError('item is not of type {}'.format(Command))
        data = command.create_json()
        try:
            r = requests.patch('{0}/command/{1}'.format(self.request,
                                                        json.loads(data)['command_name']),
                                                        auth=self.auth,
                                                        verify=False,
                                                        data=data, headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except Exception, e:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}

        return data
    
    def add_command(self, command):
        if not isinstance(command, Command):
            raise TypeError('item is not of type {}'.format(Command))
        data = command.create_json()
        try:
            r = requests.post('{0}/command'.format(self.request),
                              auth=self.auth,
                              verify=False,
                              data=data,
                              headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        return data
    
    def add_commands(self, commands):
        if not isinstance(commands, list):
            commands = [commands]
        
        self._log.put_header_to_log("Add Commands")
        for command in commands:
            if not isinstance(command, Command):
                raise TypeError('item is not of type Command')

            data = self.add_command(command)
            if self._check_error_data(data):
                log_error.LogError(log_path=self._log._log_path,
                                   to_std_out=self._log._to_std_out).put_log(data)
            else:
                self._log.put_log_add_command(Command(**data))

    def delete(self, command_name):
        try:
            r = requests.delete('{0}/command/{1}?format=json'.format(self.request, command_name),
                                auth=self.auth,
                                verify=False)
            data = json.loads(r.text)
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        self._log.put_header_to_log("Delete Command")
        if self._check_error_data(data):
            self._log.put_error_log_delete_command(command_name, data)
        else:
            self._log.put_log_delete(data)
        
    def clone(self, commands = []):
        try:
            r = requests.get('{0}/command?format=json'.format(self.request),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        command_list = CommandList()
        self._log.put_header_to_log("Clone Commands")
        for command in data:
            r = requests.get('{0}'.format(command['resource']), auth=self.auth, verify=False, headers={'content-type': 'application/json'})
            data1 = json.loads(r.text)
            if self._check_error_data(data1):
                log_error.LogError(log_path=self._log._log_path,
                                   to_std_out=self._log._to_std_out).put_log(data1)
                continue
            else:
                 self._log.put_log_clone_command(Command(**data1))

            command_list.append(Command(**data1))
        self._log.put_foot_to_log("# Count: [{0}]".format(len(command_list)))
        return command_list