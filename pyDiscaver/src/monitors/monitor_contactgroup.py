# -*- coding: UTF-8 -*-

import json

import requests
from requests.exceptions import ConnectionError

from ..contactgroup import ContactGroup, ContactGroupList
from ..log import log_error
import monitor


class MonitorContactGroup(monitor.Monitor):

    def get_contactgroup(self, contact_group):
        try:
            r = requests.get('{0}/contactgroup/{1}?format=json'.format(self.request, contact_group),
                             auth=self.auth,
                             verify=False)
            data = r.text
        except ConnectionError:
            data = json.dumps({'error': 'Connection Error', 'full_error': 'Connection time out'})

        output = json.loads(data)

        if self._check_error_data(output):
            self._log.put_error_log_get_contactgroup(contact_group, output)
        else:
            self._log.put_log_contactgroup(ContactGroup(**output))

    def list_all(self):
        try:
            r = requests.get('{0}/contactgroup?format=json'.format(self.request), auth=self.auth, verify=False)
            data = r.text
        except ConnectionError:
            data = json.dumps({'error': 'Connection Error', 'full_error': 'Connection time out'})
        output = json.loads(data)
        if self._check_error_data(output):
            log_error.LogError(log_path=self._log._log_path,
                               to_std_out=self._log._to_std_out).put_log(output)
        else:
            self._log.put_log_list_all_contactgroup(output)

    def add_contactgroup(self, contactgroup):
        if not isinstance(contactgroup, ContactGroup):
            raise TypeError('item is not of type {0}'.format(contactgroup.__name__))
        data = contactgroup.create_json()
        try:
            r = requests.post('{0}/contactgroup'.format(self.request),
                              auth=self.auth,
                              verify=False,
                              data=data,
                              headers={'content-type': 'application/json'})
            data = r.text
        except ConnectionError:
            data = json.dumps({'error': 'ConnectionError', 'full_error': 'Connection time out'})
        output = json.loads(data)
        if self._check_error_data(output):
            self._log.put_error_log_add_contactgroup(contactgroup.contactgroup_name, output)
        else:
            self._log.put_log_add_contactgroup(ContactGroup(**output))
        return output

    def add_contactgroups(self, contactgroups):
        if not isinstance(contactgroups, list):
            contactgroups = [contactgroups]
        self._log.put_header_to_log("Add Contact Groups")
        for contactgroup in contactgroups:
            output = self.add_contactgroup(contactgroup)

    def delete(self, contactgroup_name):
        try:
            r = requests.delete('{0}/contactgroup/{1}?format=json'.format(self.request, contactgroup_name),
                                auth=self.auth,
                                verify=False)
            data = json.loads(r.text)
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        self._log.put_header_to_log("Delete Contact Group")
        if self._check_error_data(data):
            self._log.put_error_log_delete_contactgroup(contactgroup_name, data)
        else:
            self._log.put_log_delete(data)

    def clone(self):
        try:
            r = requests.get('{0}/contactgroup?format=json'.format(self.request),
                            auth=self.auth, verify=False,
                            headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        contactgroup_list = ContactGroupList()
        self._log.put_header_to_log("Clone Contact Groups")
        for contactgroup in data:
            r = requests.get('{0}'.format(contactgroup['resource']),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})
            data1 = json.loads(r.text)
            if self._check_error_data(data1):
                log_error.LogError(log_path=self._log._log_path,
                                   to_std_out=self._log._to_std_out).put_log(data1)
                continue
            else:
                self._log.put_log_clone_contactgroup(ContactGroup(**data1))
            contactgroup_list.append(ContactGroup(**data1))
        self._log.put_foot_to_log("#Count: [{0}]".format(len(contactgroup_list)))
        return  contactgroup_list