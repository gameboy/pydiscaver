# -*- coding: UTF-8 -*-
import json
import urllib

import requests
from requests.exceptions import ConnectionError

from ..log import log_error
from ..service import Service, ServiceList
import monitor


class MonitorService(monitor.Monitor):
    def __init__(self, log_path = None, **kwargs):
        if 'options' in kwargs:
            self.options = kwargs.pop('options')
        super(MonitorService, self).__init__(**kwargs)

    def list_all(self):
        try:
            r = requests.get('{0}/service?format=json'.format(self.request), auth=self.auth, verify=False)
            data = r.text
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}

        output = json.loads(data)

        if self._check_error_data(output):
            log_error.LogError(log_path=self._log._log_path, to_std_out=self._log._to_std_out).put_log(output)
        else:
            self._log.put_log_list_all_services(output)

    def get_service(self, name):
        try:
            service_name = urllib.quote(urllib.quote(name, ''), '')
            r = requests.get('{0}/service/{1}?format=json'.format(self.request, service_name), auth=self.auth, verify=False)
            data = r.text
            print data
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        output = json.loads(data)
        if self._check_error_data(output):
            self._log.put_error_log_get_service(name, output)
        else:
            self._log.put_log_service(Service(**output))

    def add_services(self, services):
        if not isinstance(services, list):
            services = [services]
        self._log.put_header_to_log("Add Services")
        for service in services:
            output = self.add_service(service)

    def add_service(self, service):
        if not isinstance(service, Service):
            raise TypeError('item is not of type {}'.format(Service.__name__))
        data = service.create_json()
        try:

            r = requests.post('{0}/service'.format(self.request),
                              auth=self.auth,
                              verify=False,
                              data=data,
                              headers={'content-type': 'application/json'})
            data = r.text
        except:
            data = json.dumps({'error': 'ConnectionError', 'full_error': 'Connection time out'})
        output = json.loads(data)
        if self._check_error_data(output):
            self._log.put_error_log_add_service(service, output)
        else:
            self._log.put_log_add_service(Service(**output))
        return output

    def delete(self, service_name):
        try:
            ser_name = urllib.quote(urllib.quote(service_name, ''), '')
            r = requests.delete('{0}/service/{1}?format=json'.format(self.request, ser_name),
                                auth=self.auth,
                                verify=False)
            data = json.loads(r.text)
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        self._log.put_header_to_log("Delete Service")
        if self._check_error_data(data):
            self._log.put_error_log_delete_service(service_name, data)
        else:
            self._log.put_log_delete(service_name)

    def clone_service_by_name(self, service_name):
        try:
            r = requests.get('{0}/service?format=json'.format(self.request),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}

        service_list = ServiceList()
        new_data = []
        for service in data:
            spli = service['name'].split(';')[1]
            if not spli == service_name:
                continue
            else:
                new_data.append(service)
        data = new_data
        for service in data:
            service_name = urllib.quote(urllib.quote(service['name'], ''), '')
            r = requests.get('{0}/api/config/service/{1}'.format(self.address, service_name),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})
            output = json.loads(r.text)

            if self._check_error_data(output):
                self._log.put_error_log_clone_service(service['name'], data)
                continue
            else:
                self._log.put_log_clone_service(Service(**output))
            service_list.append(Service(**output))
        self._log.put_foot_to_log("# Count: [{0}]".format(len(service_list)))
        return service_list

    def clone(self):
        service_list = ServiceList()
        if self.options.with_service:
            for host in self.options.host_list:
                try:
                    r = requests.get('{0}/host/{1}?format=json'.format(self.request, host),
                                     auth=self.auth,
                                     verify=False,
                                     headers={'content-type': 'application/json'})
                    data = json.loads(r.text)
                except ConnectionError:
                    data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
                if 'services' not in data:
                    continue
                for service in data['services']:
                    service_name = urllib.quote(urllib.quote(service['service_description'], ''), '')
                    r = requests.get('{0}/api/config/service/{1};{2}'.format(self.address, host, service_name),
                                     auth=self.auth,
                                     verify=False,
                                     headers={'content-type': 'application/json'})
                    output = json.loads(r.text)

                    if self._check_error_data(output):
                        self._log.put_error_log_clone_service(service['service_description'], output)
                        continue
                    else:
                        self._log.put_log_clone_service(Service(**output))
                    service_list.append(Service(**output))
            self._log.put_foot_to_log("# Count: [{0}]".format(len(service_list)))
        else:
            try:
                r = requests.get('{0}/service?format=json'.format(self.request),
                                 auth=self.auth,
                                 verify=False,
                                 headers={'content-type': 'application/json'})
                data = json.loads(r.text)
            except ConnectionError:
                data = {'error': 'Connection Error', 'full_error': 'Connection time out'}


            self._log.put_header_to_log("Clone Services")
            for service in data:
                service_name = urllib.quote(urllib.quote(service['name'], ''), '')
                r = requests.get('{0}/api/config/service/{1}'.format(self.address, service_name),
                                 auth=self.auth,
                                 verify=False,
                                 headers={'content-type': 'application/json'})
                output = json.loads(r.text)

                if self._check_error_data(output):
                    self._log.put_error_log_clone_service(service['name'], data)
                    continue
                else:
                    self._log.put_log_clone_service(Service(**output))
                service_list.append(Service(**output))
            self._log.put_foot_to_log("# Count: [{0}]".format(len(service_list)))
        return service_list

    def clone_servicegroup_service(self, servicegroup_name):
        try:
            r = requests.get('{0}/servicegroup/{1}?format=json'.format(self.request, servicegroup_name),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        service_list = ServiceList()
        for service in data['members']:
            service = urllib.quote(urllib.quote(service, ''), '')
            r = requests.get('{0}/api/config/service/{1}'.format(self.address, service),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})
            output = json.loads(r.text)
            if self._check_error_data(output):
                self._log.put_error_log_clone_service(service['name'], data)
                continue
            else:
                self._log.put_log_clone_host_service(Service(**output), service.split(';')[0])
            service_list.append(Service(**output))
        self._log.put_foot_to_log("# Count: [{0}]".format(len(service_list)))
        return service_list

    def clone_host_services(self, hostname):
        try:
            r = requests.get('{0}/host/{1}?format=json'.format(self.request, hostname.decode('ISO-8859-2')),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except ConnectionError:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out'}
        print hostname.decode('utf-8')

        service_list = ServiceList()
        self._log.put_header_to_log("Clone Services For Host")
        for service in data['services']:
            service_name = urllib.quote(urllib.quote(service['service_description'], ''), '')
            r = requests.get('{0}/api/config/service/{1};{2}'.format(self.address, hostname.decode('ISO-8859-2'), service_name),
                             auth=self.auth,
                             verify=False,
                             headers={'content-type': 'application/json'})
            output = json.loads(r.text)
            if self._check_error_data(output):
                self._log.put_error_log_clone_service(service['service_description'], output)
                continue
            else:
                self._log.put_log_clone_host_service(Service(**output), hostname)

            service_list.append(Service(**output))
        self._log.put_foot_to_log("# Count: [{0}]".format(len(service_list)))
        return service_list

    def update_services(self, services):
        self._log.put_header_to_log("Update Services")
        for service in services:
            data = self.update_service(service)
            if self._check_error_data(data):
                self._log.put_error_log_update_service(service, data)
            else:
                self._log.put_log_update_service(Service(**data))

    def update_service(self, service):
        if not isinstance(service, Service):
            raise TypeError('item is not of type {}'.format(Service.__name__))
        data = service.create_json()
        try:
            ser_name = urllib.quote(urllib.quote(json.loads(data)['service_description'], ''), '')
            r = requests.patch('{0}/service/{1};{2}'.format(self.request,
                                                            json.loads(data)['host_name'],
                                                            ser_name),
                                                            auth=self.auth,
                                                            verify=False,
                                                            data=data, headers={'content-type': 'application/json'})
            data = json.loads(r.text)
        except Exception, e:
            data = {'error': 'Connection Error', 'full_error': 'Connection time out {0}'.format(e)}
        return data