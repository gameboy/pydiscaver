import json


class ServiceList(list):
    service_list = []

    def __init__(self, **kwargs):
        super(ServiceList, self).__init__(**kwargs)

    def append(self, item):
        if not isinstance(item, Service):
            raise TypeError('item is not of type {}'.format(Service))
        super(ServiceList, self).append(item)


class ServiceResources(object):
    def __init__(self, name, resource):
        self.name = name
        self. resource = resource


class Service(object):

    REQUIRED_SERVICE_PROPERTIES = ('check_command', 'check_interval', 'check_period', 'file_id', 'host_name',
                                   'max_check_attempts', 'notification_interval', 'notification_options',
                                   'notification_period', 'retry_interval', 'service_description', 'template')

    ALL_SERVICE_PROPERTIES = REQUIRED_SERVICE_PROPERTIES + ('action_url', 'active_checks_enabled', 'check_command_args',
                                                            'check_freshness', 'contact_groups', 'contacts',
                                                            'display_name', 'event_handler', 'event_handler_args',
                                                            'event_handler_enabled', 'first_notification_delay',
                                                            'flap_detection_enabled', 'flap_detection_options',
                                                            'freshness_threshold', 'high_flap_threshold',
                                                            'hostgroup_name', 'icon_image', 'icon_image_alt',
                                                            'initial_state', 'is_volatile', 'low_flap_threshold',
                                                            'notes', 'notes_url', 'notifications_enabled', 'obsess',
                                                            'parallelize_check', 'passive_checks_enabled',
                                                            'process_perf_data', 'register',
                                                            'retain_nonstatus_information', 'retain_status_information',
                                                            'servicegroups', 'stalking_options', )

    TO_SPLIT_PROPERTIES = ('notification_options', 'contact_groups', 'contacts', 'flap_detection_options',
                           'servicegroups', 'stalking_options')

    def __init__(self, **kwargs):
        self.custom_variables = {}
        for key, value in kwargs.iteritems():
            if key in Service.ALL_SERVICE_PROPERTIES:
                if key in Service.TO_SPLIT_PROPERTIES:
                    if not isinstance(value, list):
                        setattr(self, key, value)
                        continue
                setattr(self, key, value)
            if key.startswith('_'):
                self.custom_variables[key] = value

    def create_json(self):
        data = {}
        for key, value in self.__dict__.iteritems():
            if key == 'custom_variables':
                continue
            data[key] = value
        for key, value in self.custom_variables.iteritems():
            data[key] = value
        return json.dumps(data)

    def print_custom_variables(self):
        for key, value in self.custom_variables.iteritems():
            print("{0} = {1}".format(key, value))

    def __str__(self):
        return "Service: {0}".format(self.service_description)