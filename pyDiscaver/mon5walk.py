# -*- coding: UTF-8 -*-
from src.options import parser
from src import mon5walkOptions
from src import mon5walk
import sys

reload(sys)
sys.setdefaultencoding("utf-8")

#import datetime
#end_date = datetime.date(2013, 12, 1)
#today_date = datetime.date.today()
#difference = end_date - today_date
#if difference < datetime.timedelta(0):
#    print "End free time. Please buy full version"
#    sys.exit(0)

options = mon5walkOptions.Mon5WalkOptions(parser)
mon5walk = mon5walk.Mon5Walk(options)
mon5walk.run()

sys.exit()

